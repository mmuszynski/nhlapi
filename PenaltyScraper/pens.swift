//
//  pens.swift
//  PenaltyScraper
//
//  Created by Mike Muszynski on 10/21/20.
//  Copyright © 2020 Mike Muszynski. All rights reserved.
//

import Foundation


//let folderName = args[1]

//let range = Calendar.current.date(from: DateComponents(year: 2001))!...Calendar.current.date(from: DateComponents(year: 2002))!
//let gameURL = Endpoint.schedule(dateRange: range).url
//
//var components = URLComponents(url: gameURL, resolvingAgainstBaseURL: false)
//var items = components?.queryItems
//items?.append(URLQueryItem(name: "expand", value: "schedule.linescore"))
//components?.queryItems = items
//
//let url = components?.url


//class PenaltyTracker {
//    var year: Int = 2001
//    var games: Int = 0
//    var dictionary: [String : Int] = [:]
//    func tally(_ penaltyName: String) {
//        if !dictionary.keys.contains(penaltyName) {
//            dictionary[penaltyName] = 0
//        }
//
//        dictionary[penaltyName]! += 1
//    }
//
//    var csvOutput: String {
//        var csv = ""
//
//        for key in self.dictionary.keys.sorted(by: <) {
//            csv += "\r\(year), \"\(key)\", \(self.dictionary[key] ?? 0)"
//        }
//
//        return csv
//    }
//}
//
////let url = URL(fileURLWithPath: FileManager.default.currentDirectoryPath).appendingPathComponent(folderName)
//let baseURL = URL(fileURLWithPath: "/Users/mike/Desktop/NHL")
//
//var trackers: [PenaltyTracker] = []
//
//var validYears = Array(stride(from: 2001, to: 2020, by: 1))
//validYears.removeAll(where: { $0 == 2004 })
//for year in validYears {
//
//    let url = baseURL.appendingPathComponent("\(year)")
//
//    let tracker = PenaltyTracker()
//    tracker.year = year
//
//    var count = 0
//    var currentFilePath: String = ""
//
//    print("Starting year \(year)")
//
//    do {
//        let files = try FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: .skipsHiddenFiles).sorted(by: { $0.path < $1.path } )
//
//        for fileURL in files  {
//            do {
//                let result: LiveFeedResult = try NHLAPI.decode(from: try Data(contentsOf: fileURL))
//                currentFilePath = fileURL.path
//
//                //get penalties
//                for play in result.allPlays.filter({ $0.playType == .penalty }) {
//                    guard let penalty = play.result.secondaryType else { continue }
//                    tracker.tally(penalty)
//                }
//
//                count += 1
//                if count % 100 == 0 {
//                    print("\(count)... ", terminator: "")
//                }
//
//            } catch {
//                print("error in \(currentFilePath)")
//                print("\(error)")
//            }
//        }
//
//        tracker.games = count
//
//        print("Finished year \(year). \(count) games analyzed")
//        trackers.append(tracker)
//    } catch {
//        fatalError("\(error)")
//    }
//
//}
//
//let outputURL = baseURL.appendingPathComponent("penaltyAnalysis.csv")
//
//let allPens = trackers.reduce([]) { (result, tracker) -> [String] in
//    Array(Set(result + tracker.dictionary.keys))
//}
//
//var trackersByYear = [String: PenaltyTracker]()
//for tracker in trackers {
//    trackersByYear["\(tracker.year)"] = tracker
//}
//
//var csv = "year, name, count, pergame"
//for pen in allPens.sorted() {
//    for year in validYears {
//        let yearstring = "\(year)"
//
//        let tracker = trackersByYear[yearstring]
//        let count = tracker?.dictionary[pen] ?? 0
//        let games = tracker?.games ?? 1
//        let pergame = Double(count) / Double(games)
//        csv += "\r" + yearstring + "," +  pen + "," +  "\(count)" + "," + "\(pergame)"
//    }
//}
//
//do {
//    try csv.write(to: outputURL, atomically: true, encoding: .utf8)
//} catch {
//    print("couldn't output file: \(error)")
//}
