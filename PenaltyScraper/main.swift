//
//  main.swift
//  PenaltyScraper
//
//  Created by Mike Muszynski on 8/28/20.
//  Copyright © 2020 Mike Muszynski. All rights reserved.
//

import Foundation
import NHLAPI

let args = CommandLine.arguments

//try checkForUnassistedGoals()

let results = try loadResults(from: resultsURL)
//https://www.nhl.com/gamecenter/ana-vs-phx/1997/04/22/1996030184#game=1996030184,game_state=final

var cachedTeamResults: [URL : Team] = [:]

let baseURL = URL(string: "https://statsapi.web.nhl.com")!

func getTeamInfo(for teamURL: URL) throws -> Team {
    //print("loading teamURL \(teamURL)")
    if let team = cachedTeamResults[teamURL] {
        return team
    }
    
    let data = try Data(contentsOf: teamURL)
    let result = try JSONDecoder().decode(TeamsResult.self, from: data)
    
    cachedTeamResults[teamURL] = result.first!
    return result.first!
}

struct GameRecap {
    var id: Int
    var homeGoals: Int
    var awayGoals: Int
    var homeTeam: Team
    var awayTeam: Team
    var date: Date
    
    var isPreseason: Bool {
        id % Int(10e5) < 20000
    }
    
    var gamecenterLink: String {
        let components = Calendar.current.dateComponents([.day, .month, .year], from: date)
        
        let numberFormatter = NumberFormatter()
        numberFormatter.minimumIntegerDigits = 2
        numberFormatter.paddingCharacter = "0"
        
        var urlString = "https://www.nhl.com/gamecenter/\(awayTeam.abbreviation!.lowercased())-vs-\(homeTeam.abbreviation!.lowercased())/"
        urlString += numberFormatter.string(from: NSNumber(value: components.year!))! + "/"
        urlString += numberFormatter.string(from: NSNumber(value: components.month!))! + "/"
        urlString += numberFormatter.string(from: NSNumber(value: components.day!))! + "/"
        urlString += "\(id)"
        
        return urlString
    }
    
    var redditOutputLine: String {
        let homeTeamFullName = homeTeam.name
        let awayTeamFullName = awayTeam.name
        
        let score = "\(awayGoals)-\(homeGoals)"
        let scoreLink = "[\(score)](\(gamecenterLink))"
        
        let dateOutput = ISO8601DateFormatter().string(from: date).components(separatedBy: "T").first!
        return "\(dateOutput)|\(scoreLink)|\(awayTeamFullName)|\(homeTeamFullName)"
    }
}

var recaps: [GameRecap] = []

func parse(_ gameID: Int) throws {
    let url = Endpoint.liveFeed(game: gameID).url
    let data = try Data(contentsOf: url)
    let feed = try JSONDecoder().decode(LiveFeedResult.self, from: data)
    
    let awayURL = URL(string: feed.awayTeam.link, relativeTo: baseURL)!.absoluteURL
    let homeURL = URL(string: feed.homeTeam.link, relativeTo: baseURL)!.absoluteURL
    
    let away = try getTeamInfo(for: awayURL)
    let home = try getTeamInfo(for: homeURL)
    
    let date = feed.dateTime
    
    let inputFormatter = ISO8601DateFormatter()
    guard let dateObject = inputFormatter.date(from: date) else {
        print("couldn't get date components for date: \(date)")
        return
    }
    
    var gameRecap = GameRecap(id: gameID,
                              homeGoals: feed.boxscore.homeTeamGoals,
                              awayGoals: feed.boxscore.awayTeamGoals,
                              homeTeam: home,
                              awayTeam: away,
                              date: dateObject)
    
    recaps.append(gameRecap)
}

for result in results.filter({ $0.year >= 1945 }) {
    for game in result.games {
        try parse(game)
    }
}

print("###More than one goal")
print("Date|Score|Away|Home")
print(":--|:-:|:--|:--")

recaps = recaps.sorted(by: { $0.id < $1.id } ).reversed()

for recap in recaps.filter({ !$0.isPreseason && ($0.homeGoals + $0.awayGoals) > 1 }) {
    print(recap.redditOutputLine)
}

print("\r")
print("###1-0 final scores")
print("Date|Score|Away|Home")
print(":--|:-:|:--|:--")

for recap in recaps.filter({ !$0.isPreseason && ($0.homeGoals + $0.awayGoals) == 1 }) {
    print(recap.redditOutputLine)
}
