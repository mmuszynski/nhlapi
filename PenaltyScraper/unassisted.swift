//
//  unassisted.swift
//  PenaltyScraper
//
//  Created by Mike Muszynski on 10/21/20.
//  Copyright © 2020 Mike Muszynski. All rights reserved.
//

import Foundation
import NHLAPI

struct ResultsYear: Codable {
    var year: Int
    var games: [Int]
}

let resultsURL = URL(fileURLWithPath: "/Users/mike/Desktop/unassistedGames.json")

func loadResults(from url: URL) throws -> [ResultsYear] {
    let resultsData: Data
    do {
        resultsData = try Data(contentsOf: resultsURL)
    } catch {
        let emptyArray = [ResultsYear]()
        try save(results: emptyArray, to: url)
        return try loadResults(from: url)
    }
    return try JSONDecoder().decode([ResultsYear].self, from: resultsData)
}

func save(results: Array<ResultsYear>, to url: URL) throws {
    let data = try JSONEncoder().encode(results)
    try data.write(to: url)
}

func checkForUnassistedGoals() throws {
    
    var resultsArray = try loadResults(from: resultsURL)
    let firstYear = (resultsArray.map { $0.year }.sorted(by: <).last ?? 1916) + 1
    
    for year in stride(from: firstYear, to: 2020, by: 1) {
        if year == 2004 { continue }
        
        print("Starting year \(year)...")
        let beginMonth: Int = { switch year {
        case 2016:
            return 10
        default:
            return 9
            }
        }()
        let beginDay: Int = { switch year {
        case 2016:
            return 10
        default:
            return 1
            }
        }()
        
        let endMonth: Int = { switch year {
        case 2019:
            return 9
        default:
            return 8
            }}()
        
        let endDay: Int = { switch year {
        case 2019:
            return 30
        default:
            return 31
            }}()
        let beginDate = Calendar.current.date(from: DateComponents(year: year, month: beginMonth, day: beginDay))!
        let endDate = Calendar.current.date(from: DateComponents(year: year + 1, month: endMonth, day: endDay))!
        
        let url = Endpoint.schedule(dateRange: beginDate...endDate).url
        print(url)
        
        let data = try! Data(contentsOf: url)
        let result = try! JSONDecoder().decode(ScheduleResult.self, from: data)
        
        var gameIDs = [Int]()
        
        for day in result.dates {
            //print("Checking date \(day.date)")
            for game in day.games {
                let boxURL = Endpoint.boxscore(id: game.id).url
                let data = try! Data(contentsOf: boxURL)
                let box = try JSONDecoder().decode(Boxscore.self, from: data)
                
                if box.awayTeamAssists + box.homeTeamAssists == 0 && box.homeTeamGoals + box.awayTeamGoals != 0 {
                    print("Found a game with zero assists on \(day.date): \(game.id)")
                    gameIDs.append(game.id)
                }
            }
        }
        
        let results = ResultsYear(year: year, games: gameIDs)
        resultsArray.append(results)
        try save(results: resultsArray, to: resultsURL)
    }
    
}
