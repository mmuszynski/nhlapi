//
//  CacheTest.swift
//  
//
//  Created by Mike Muszynski on 6/26/22.
//

import XCTest
@testable import NHLAPI

final class CacheTest: XCTestCase {
    
    func testCaching() async {
        do {
            Downloader.debug = true
            let _ = try await Schedule.today
            let _ = try await Schedule.today
            Downloader().purgeCache()
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }
    
}
