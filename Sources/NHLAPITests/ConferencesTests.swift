//
//  ConferencesTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class ConferencesTests: NHLAPITestCase {
    
    func testDecodeConferencesResult() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "ConferencesResult")
            let result = try NHLAPI.decode(ConferencesResult.self, from: data)
            let west = result.first
            XCTAssertEqual(west?.name, "Western")
        }
    }

}
