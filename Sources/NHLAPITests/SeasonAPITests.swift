//
//  NHLAPITests.swift
//  NHLAPITests
//
//  Created by Mike Muszynski on 2/13/21.
//  Copyright © 2021 Mike Muszynski. All rights reserved.
//

import XCTest
import Combine
@testable import NHLAPI

class SeasonTests: NHLAPITestCase {
    
    func testSeasonResultDecode() {
        let jsonData = try! self.jsonDataFromFile(named: "SeasonResultExample")
        tryAPIDecode {
            let result = try NHLAPI.decoder.decode(SeasonsResult.self, from: jsonData)
            XCTAssertEqual(result.seasons.count, 1)
            XCTAssertEqual(result.copyright, "NHL and the NHL Shield are registered trademarks of the National Hockey League. NHL and NHL team marks are the property of the NHL and its teams. © NHL 2019. All Rights Reserved.")
            
            let startDate = Date(year: 2017, month: 10, day: 4)
            let endDate = Date(year: 2018, month: 4, day: 8)

            XCTAssertEqual(result.seasons.first?.regularSeasonStartDate, startDate)            
            XCTAssertEqual(result.seasons.first?.regularSeasonEndDate, endDate)
            
            let seasonEndDate = Calendar.current.date(from: DateComponents(year: 2018, month: 6, day: 7))
            XCTAssertEqual(result.seasons.first?.seasonEndDate, seasonEndDate)
        }
    }
    
    var cancellables = Set<AnyCancellable>()
    
    func testPenalties() async throws {
        let result = try await Season(forYear: 2022)
    }

}
