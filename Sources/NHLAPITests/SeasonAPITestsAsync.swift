//
//  NHLAPITests.swift
//  NHLAPITests
//
//  Created by Mike Muszynski on 2/13/21.
//  Copyright © 2021 Mike Muszynski. All rights reserved.
//

import XCTest
import Combine
@testable import NHLAPI

class SeasonTestsAsync: NHLAPIAsynchronousTestCase {
    
    func testAllSeasonPublisher() {
        testAsynchronously(timeout: 5) { expectation in
            asyncCancellable = SeasonResult.publisherForAllSeasons()
                .sink(receiveCompletion: basicSinkCompletion) { seasons in
                    XCTAssert(seasons.count > 10)
                    expectation.fulfill()
                }
        }
    }
    
    func testCurrentSeasonPublisher() {
        testAsynchronously(timeout: 5) { ex in
            asyncCancellable = SeasonResult.publisherForCurrentSeason()
                .sink(receiveCompletion: basicSinkCompletion) { season in
                    ex.fulfill()
                }
        }
    }
    
    func testSpecificSeasonPublisher() {
        testAsynchronously(timeout: 5) { ex in
            asyncCancellable = SeasonResult.publisher(forSeason: "20182019")
                .sink(receiveCompletion: basicSinkCompletion) { season in
                    XCTAssertNotNil(season)
                    ex.fulfill()
                }
        }
        
    }
    
    func testSeasonPublisherForLockout() {
        testAsynchronously(timeout: 5) { ex in
            asyncCancellable = SeasonResult.publisher(forSeason: "20042005")
                .sink(receiveCompletion: { completion in
                    if case let .failure(error) = completion {
                        XCTAssertEqual(error as? SeasonResult.FetchError, SeasonResult.FetchError.noSeason)
                    } else {
                        XCTFail("Unexpected error")
                    }
                    ex.fulfill()
                }, receiveValue: { season in
                    XCTAssertNil(season)
                    ex.fulfill()
                })
        }
    }
    
    func testSeasonGamesPublisher() {
        testAsynchronously(timeout: 30) { ex in
            asyncCancellable = SeasonResult.publisher(forSeason: "20032004")
                .flatMap { season in
                    season.publisherForScheduledGames()
                }
                .sink(receiveCompletion: basicSinkCompletion) { games in
                    XCTAssertEqual(games.count, 1231)
                    ex.fulfill()
                }

        }
    }
}
