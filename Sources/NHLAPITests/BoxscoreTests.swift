//
//  TeamsTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class BoxscoreTests: NHLAPITestCase {
    
    func testDecodeBoxscoreResult() {
        tryAPIDecodeJSON(named: "BoxscoreExample", type: BoxscoreResult.self) { box in
            XCTAssertEqual(box.awayTeamGoalies.count, 1)
            XCTAssertEqual(box.homeTeamGoalies.count, 1)
        }
    }
    
    func testChasedGoalie() {
        tryAPIDecodeJSON(named: "BoxscoreChasedGoalie", type: BoxscoreResult.self) { box in
            XCTAssertEqual(box.awayTeamGoalies.count, 2)
            XCTAssertEqual(box.homeTeamGoalies.count, 1)
        }
    }

}
