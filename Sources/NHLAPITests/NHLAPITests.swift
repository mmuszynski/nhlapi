//
//  NHLAPITests.swift
//  NHLAPITests
//
//  Created by Mike Muszynski on 11/9/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import XCTest
import Combine
@testable import NHLAPI

class NHLAPITestCase: XCTestCase {
    enum URLError: Error {
        case couldNotFindJSONData(named: String)
    }
    
    let downloader = Downloader()
    let decoder = NHLAPI.decoder
    let encoder = JSONEncoder()
    
    func jsonDataFromFile(named name: String) throws -> Data {
        guard let url = Bundle.module.url(forResource: name, withExtension: "json") else {
            throw URLError.couldNotFindJSONData(named: name)
        }
        return try Data(contentsOf: url)
    }
    
    func tryAPIDecodeJSON<T: Decodable>(named name: String, type: T.Type, block: (T) throws -> Void) {
        do {
            let data = try jsonDataFromFile(named: name)
            let result = try NHLAPI.decode(T.self, from: data)
            try block(result)
        } catch DecodingError.keyNotFound(let key, let context) {
            printErrorInformation(with: key, context: context)
            XCTFail(context.debugDescription)
        } catch DecodingError.typeMismatch(let type, let context) {
            print(type)
            XCTFail(context.debugDescription)
        } catch {
            XCTFail("Unexpected error: \(error)")
        }
    }
    
    func tryAPIDecode(_ block: () throws -> Void) {
        do {
            try block()
        } catch DecodingError.keyNotFound(let key, let context) {
            printErrorInformation(with: key, context: context)
            XCTFail(context.debugDescription)
        } catch DecodingError.typeMismatch(let type, let context) {
            print(type)
            XCTFail(context.debugDescription)
        } catch {
            XCTFail("Unexpected error: \(error)")
        }
    }
    
    func printErrorInformation(with key: CodingKey, context: DecodingError.Context) {
        let line = String(repeating: "=", count: 40)
    
        print(line)
        print("DECODING ERROR:")
        print("Key '" + key.stringValue + "' not found in" + context.codingPath.reduce("") { string, key in
            return string + " >\(key.stringValue)"
            })
        print(line)
    }
    
}

class NHLAPIAsynchronousTestCase: NHLAPITestCase {
    var asyncCancellable: AnyCancellable?
    var cancellables: Set<AnyCancellable> = []
    func testAsynchronously(timeout interval: TimeInterval, _ block: (XCTestExpectation)->()) {
        let ex = expectation(description: "Wait for async")
        block(ex)
        wait(for: [ex], timeout: interval)
    }
    
    func basicSinkCompletion(completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            break
        case .failure(let error):
            XCTFail("Unexpected error: \(error)")
        }
    }
}
