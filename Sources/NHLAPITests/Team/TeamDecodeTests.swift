//
//  TeamsTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class TeamTests: NHLAPITestCase {
    
    func testDecodeTeamContent() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "TeamResultTBL")
            let teamResult = try NHLAPI.decode(TeamsResult.self, from: data)
            let lightning = teamResult.first
            XCTAssertNotNil(lightning)
        }
    }

}
