//
//  TeamAsyncTests.swift
//  
//
//  Created by Mike Muszynski on 6/21/22.
//

import XCTest
@testable import NHLAPI

final class TeamAsyncTests: XCTestCase {
    
    func testTeamAsync() async {
        do {
            let team = try await Teams.fetchTeamWith(id: 1)
            
            XCTAssertEqual(team.name, "New Jersey Devils")
            XCTAssertEqual(team.firstYearOfPlay, "1982")
            XCTAssertEqual(team.active, true)
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }
    
}
