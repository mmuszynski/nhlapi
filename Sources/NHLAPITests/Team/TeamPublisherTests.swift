//
//  TeamsTests.swift
//  
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class TeamTestsAsync: NHLAPIAsynchronousTestCase {
    
    func testTeamPublisher() {
        testAsynchronously(timeout: 10) { ex in
            asyncCancellable = TeamResult
                .publisher(ids: Array(stride(from: 1, to: 100, by: 1)))
                .sink(receiveCompletion: basicSinkCompletion) { teamArray in
                    XCTAssertTrue(!teamArray.isEmpty)
                    ex.fulfill()
                }
        }
    }

}
