//
//  TeamsTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class FranchiseTests: NHLAPITestCase {
    
    func testDecodeFranchiseResult() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "FranchisesResult")
            let teamResult = try NHLAPI.decode(FranchisesResult.self, from: data)
            let lightning = teamResult.first
            XCTAssertNotNil(lightning)
        }
    }

}
