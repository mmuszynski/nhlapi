//
//  VenueTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class ScheduleTests: NHLAPITestCase {
    
    func testScheduleDecode() {
        let now = Date(timeIntervalSinceReferenceDate: 595025715.386615)
        
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "ScheduleResultExample")
            let schedule = try NHLAPI.decode(ScheduleResult.self, from: data)
            let todaysGames = schedule[now]
            XCTAssertNotNil(todaysGames)
            XCTAssertEqual(todaysGames!.games.count, schedule.totalGames)
            
            let game = todaysGames?.first
            XCTAssertNotNil(game)
            
            XCTAssertEqual(game?.homeTeam.name, "New York Islanders")
            XCTAssertEqual(game?.awayTeam.name, "Florida Panthers")
        }
    }
    
    func testMoreScheduleItems() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "ScheduleResultExample")
            let schedule = try NHLAPI.decode(ScheduleResult.self, from: data)
            let first = schedule.allGames.first
            XCTAssertEqual(first?.homeScore, 2)
            XCTAssertEqual(first?.awayScore, 1)
            XCTAssertEqual(first?.awayTeamID, 13)
            XCTAssertEqual(first?.homeTeamID, 2)
            XCTAssertEqual(first?.homeTeamOutcome, .undecided)
            XCTAssertEqual(first?.awayTeamOutcome, .undecided)
            
            //2019-11-09T18:00:00Z
            let components = DateComponents(year: 2019, month: 11, day: 9, hour: 13)
            let date = Calendar.current.date(from: components)
            XCTAssertEqual(date, first?.gameDate)
        }
        
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "ScheduleResultCompleted")
            let schedule = try NHLAPI.decode(ScheduleResult.self, from: data)
            let first = schedule.allGames.first!
            XCTAssertEqual(first.homeTeamOutcome, .loss)
            XCTAssertEqual(first.awayTeamOutcome, .win)
        }
    }
    
    func testWithLinescoreExpand() {
        tryAPIDecodeJSON(named: "ScheduleResultExpandLinescore", type: ScheduleResult.self) { result in
            XCTAssertNotNil(result.allGames.first?.linescore)
        }
    }
}
