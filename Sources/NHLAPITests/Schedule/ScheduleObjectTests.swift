//
//  ScheduleObjectTests.swift
//  
//
//  Created by Mike Muszynski on 6/25/22.
//

import XCTest
@testable import NHLAPI

internal extension Date {
    init?(year: Int? = nil, month: Int? = nil, day: Int? = nil, hour: Int? = nil, minute: Int? = nil, second: Int? = nil) {
        let components = DateComponents(year: year, month: month, day: day, hour: hour, minute: minute, second: second)
        guard let date = Calendar.current.date(from: components) else { return nil }
        self = date
    }
}

final class ScheduleObjectTests: XCTestCase {

    func testScheduleObject() async {
        do {
            Downloader.debug = true
            let date = Date(year: 2022, month: 06, day: 20)!
            let schedule = try await Schedule.forWeek(containing: date)
            XCTAssertEqual(schedule[date]?.count, 1)
        } catch {
            XCTFail("unexpected error: \(error)")
        }
    }
    
    func testScheduleObjectDate() async {
        //Many of the results for September are bad as well.
        
        do {
            Downloader.debug = true
            
            let date = Date(year: 2020, month: 09, day: 14)!
            let schedule = try await Schedule.forDay(containing: date)
            XCTAssertEqual(schedule[date]?.count, 1)
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }

}
