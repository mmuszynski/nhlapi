//
//  VenueTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class ScheduleTestsAsync: NHLAPIAsynchronousTestCase {
    
    override func setUp() {
        Downloader.debug = true
    }
    
    override func tearDown() {
        Downloader.debug = false
    }
    
    func testTodaysSchedulePublisher() {
        testAsynchronously(timeout: 10) { (ex) in
            asyncCancellable = ScheduleGameResult
                .publisher()
                .sink(receiveCompletion: basicSinkCompletion,
                      receiveValue: { (_) in
                        ex.fulfill()
                      })
        }
    }
        
    func testSpecificDaySchedule() {
        
        let september14_2020 = Date(year: 2020, month: 9, day: 14)!
        
        testAsynchronously(timeout: 10) { (ex) in
            asyncCancellable = ScheduleGameResult
                .publisher(for: september14_2020)
                .sink(receiveCompletion: basicSinkCompletion,
                      receiveValue: { (gameResults) in                        XCTAssertEqual(gameResults.first?.homeTeam.name, "Vegas Golden Knights")
                        XCTAssertEqual(gameResults.first?.awayTeam.name, "Dallas Stars")
                        ex.fulfill()
                      })
        }
    }
    
    func testDateRangeSchedule() {
        //Again, dates for this range do not return the correct game information
        //The entire SCF is missing for this season as of 6/28/22
        
        let dateComponentsBegin = DateComponents(year: 2020, month: 9, day: 19)
        let dateComponentsEnd = DateComponents(year: 2020, month: 9, day: 28)
        let start = Calendar.current.date(from: dateComponentsBegin)!
        let end = Calendar.current.date(from: dateComponentsEnd)!
        let scf2020dateRange = start...end
        
        testAsynchronously(timeout: 10) { (ex) in
            asyncCancellable = ScheduleGameResult
                .publisher(for: scf2020dateRange)
                .sink(receiveCompletion: basicSinkCompletion,
                      receiveValue: { (gameResults) in
                        XCTAssertEqual(gameResults.first?.awayTeam.name, "Dallas Stars")
                        XCTAssertEqual(gameResults.first?.homeTeam.name, "Tampa Bay Lightning")
                        XCTAssertEqual(gameResults.count, 6)
                        ex.fulfill()
                      })
        }
    }
    
    func testMoreDaySchedules() {
        testAsynchronously(timeout: 10) { ex in
            let components = DateComponents(year: 2003, month: 12, day: 31)
            let date = Calendar.current.date(from: components)!
            ScheduleGameResult
                .publisher(for: date)
                .sink(receiveCompletion: basicSinkCompletion) { (results) in
                    XCTAssertEqual(results.count, 10)
                    ex.fulfill()
                }
                .store(in: &cancellables)
        }
    }
    
    func testLinescoreExpand() {
        testAsynchronously(timeout: 10) { ex in
            let components = DateComponents(year: 2003, month: 12, day: 31)
            let date = Calendar.current.date(from: components)!
            ScheduleGameResult
                .publisher(for: date, expanding: [.linescore])
                .sink(receiveCompletion: basicSinkCompletion) { (results) in
                    XCTAssertNotNil(results.first?.linescore)
                    ex.fulfill()
                }
                .store(in: &cancellables)
        }
    }
    
    func testTeamSchedule() {
        //The date-based schedule endpoint is particularly fragile
        //This has been correct in the past but is wrong as of 6/28/22
                
        //The Tampa Bay Lightning (id: 14) played 82 regular season games in 2018-19
        testAsynchronously(timeout: 10) { ex in
            var numGames: Int = 0
            
            SeasonResult.publisher(forSeason: "20182019")
                .flatMap { seasonResult -> AnyPublisher<Array<ScheduleGameResult>, Error> in
                    let regularSeason = seasonResult.regularSeasonStartDate...seasonResult.regularSeasonEndDate
                    numGames = seasonResult.numberOfGames
                    return ScheduleGameResult
                        .publisher(forTeam: 14, dateRange: regularSeason)
                        .eraseToAnyPublisher()
                }
                .replaceError(with: [])
                .sink { games in
                    XCTAssertEqual(games.count, numGames)
                    ex.fulfill()
                }
                .store(in: &cancellables)
        }
        
        //They played four more playoff games (oof)
        testAsynchronously(timeout: 10) { ex in
            SeasonResult.publisher(forSeason: "20182019")
                .flatMap { seasonResult -> AnyPublisher<Array<ScheduleGameResult>, Error> in
                    let regularSeason = seasonResult.regularSeasonStartDate...seasonResult.seasonEndDate
                    return ScheduleGameResult
                        .publisher(forTeam: 14, dateRange: regularSeason)
                        .eraseToAnyPublisher()
                }
                .replaceError(with: [])
                .sink { games in
                    XCTAssertEqual(games.count, 86)
                    ex.fulfill()
                }
                .store(in: &cancellables)
        }
    }
    
}
