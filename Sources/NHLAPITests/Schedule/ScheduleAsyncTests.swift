//
//  ScheduleAsyncTests.swift
//  
//
//  Created by Mike Muszynski on 6/21/22.
//

import XCTest
@testable import NHLAPI

final class ScheduleAsyncTests: XCTestCase {

    func testScheduleAwait() async {
        do {
            Downloader.debug = true
            Downloader().purgeCache()
            let _ = try await Games.today
            let _ = try await Games.yesterday
        } catch {
            XCTFail("Unexpected error: \(error)")
        }
    }
    
    func testScheduleThatShouldHaveAGame() async {
        //for some reason, this API call is not correct at all
        
        do {
            // SCF Game 5 2021-07-07
            let june_7_2021 = Date(year: 2021, month: 7, day: 7)!
            let scf = try await Schedule.forDay(containing: june_7_2021)
            
            XCTAssertEqual(scf.gameCount, 1)
            let games = scf[june_7_2021]
            
            XCTAssertEqual(games?.first?.homeScore, 1)
            XCTAssertEqual(games?.first?.awayScore, 0)
            XCTAssertEqual(games?.first?.homeTeam.name, "Tampa Bay Lightning")
            XCTAssertEqual(games?.first?.awayTeam.name, "Montréal Canadiens")
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }
    
    func testScheduleWithNoGames() async {
        do {
            let components = DateComponents(year: 2022, month: 6, day: 25)
            let offendingDate = Calendar.current.date(from: components)!
            let games = try await Games.forDay(containing: offendingDate)
            XCTAssert(games.isEmpty)
        } catch {
            XCTFail("Unexpected error: \(error)")
        }
    }
    
//    func testScheduleObject() async {
//        do {
//            let _ = try await Schedule.forDay(containing: try Date(""))
//        } catch {
//            XCTFail("Unexpected error: \(error)")
//        }
//    }

}
