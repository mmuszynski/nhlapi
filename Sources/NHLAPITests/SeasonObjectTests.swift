//
//  SeasonObjectTests.swift
//  NHLAPITests
//
//  Created by Mike Muszynski on 2/27/21.
//  Copyright © 2021 Mike Muszynski. All rights reserved.
//

import XCTest
import Combine
@testable import NHLAPI

class SeasonObjectTests: NHLAPIAsynchronousTestCase {
    
    var asyncStorage: Set<AnyCancellable> = []
    
    /// Get the season information with the new Season
    func testSeasonInformation() async {
        do {
            let season = try await Season(forYear: 2003)
            XCTAssertEqual(season.regularSeasonStartDate, Date(year: 2003, month: 10, day: 8))
            XCTAssertEqual(season.regularSeasonEndDate, Date(year: 2004, month: 4, day: 4))
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }
    
    func testLockoutNoSeason() async {
        do {
            let _ = try await Season(forYear: 2004)
            XCTFail("Should throw")
        } catch {
            XCTAssertEqual(Season.FetchError.noSeason, error as? Season.FetchError)
        }
    }
    
    func testSeasonSchedule() async {
        do {
            let season = try await Season(forYear: 2003)
            let schedule = try await season.fullRegularSeasonSchedule
            XCTAssertEqual(schedule.gameCount, 1231)
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }
    
    func testGetGameFromSeasonSchedule() async {
        do {
            let season = try await Season(forYear: 2003)
            let schedule = try await season.fullRegularSeasonSchedule
            let game = schedule.allGames.first
            XCTAssertEqual(game?.homeTeam.name, "Dallas Stars")
            XCTAssertEqual(game?.awayTeam.name, "Anaheim Ducks")
            XCTAssertEqual(game?.homeScore, 4)
            XCTAssertEqual(game?.awayScore, 1)
            
            let date = DateComponents(calendar: .current, timeZone: TimeZone.current, year: 2003, month: 10, day: 8, hour: 19).date
            XCTAssertEqual(game?.date, date)
        } catch {
            XCTFail("Unexpected error \(error)")
        }
    }

}
