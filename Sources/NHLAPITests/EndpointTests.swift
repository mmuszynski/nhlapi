//
//  ConferencesTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class EndpointTests: NHLAPITestCase {
    
    func testScheduleEndpoint() {
        XCTAssertEqual(Endpoint.schedule().url.absoluteString,
                       "https://statsapi.web.nhl.com/api/v1/schedule")
        
        let startComponents = DateComponents(year: 2001, month: 1, day: 1)
        let startDate = Calendar.current.date(from: startComponents)!
        
        let endComponents = DateComponents(year: 2001, month: 1, day: 31)
        let endDate = Calendar.current.date(from: endComponents)!
        
        XCTAssertEqual(Endpoint.schedule(dateRange: startDate...endDate).url.absoluteString,
                       "https://statsapi.web.nhl.com/api/v1/schedule?startDate=2001-01-01&endDate=2001-01-31")
    }
    
    func testVenueEndpoint() {
        XCTAssertEqual(Endpoint.venue(id: 5076).url.absoluteString,
                       "https://statsapi.web.nhl.com/api/v1/venues/5076")
        XCTAssertEqual(Endpoint.venues.url.absoluteString,
                       "https://statsapi.web.nhl.com/api/v1/venues")
    }
    
    func testScheduleWithExpands() {
        let theURL = Endpoint.schedule().url(expanding: [ScheduleResult.Expansion.linescore])
        XCTAssertEqual(theURL.absoluteString, "https://statsapi.web.nhl.com/api/v1/schedule?expand=schedule.linescore")
    }
    
    func testWeeklySchedule() {
        let string = "https://statsapi.web.nhl.com/api/v1/schedule?startDate=2022-06-19&endDate=2022-06-25"
        let endpoint = Endpoint.weeklySchedule(containing: Date(year: 2022, month: 6, day: 22))
        XCTAssertEqual(endpoint.url.absoluteString, string)
    }
    
    func testAllCases() {
        let cases = Endpoint.customizableCases
    }

}
