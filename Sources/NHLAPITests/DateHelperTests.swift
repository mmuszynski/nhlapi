//
//  DateHelperTests.swift
//  
//
//  Created by Mike Muszynski on 6/25/22.
//

import XCTest
@testable import NHLAPI

final class DateHelperTests: XCTestCase {
    
    var testDate: Date?
        
    override func setUp() {
        self.testDate = Date(year: 2022, month: 06, day: 25)
    }
    
    override func tearDown() {
        NHLAPI.shortDateFormatter.timeZone = Calendar.current.timeZone
    }
    
    func testBeginningOfDay() {
        print(Date().timeIntervalSinceReferenceDate)
        XCTAssertEqual(
            Date(timeIntervalSinceReferenceDate: 677876053.640059).beginningOfDay,
            Date(year: 2022, month: 06, day: 25)
        )
        
        XCTAssertEqual(
            Date(timeIntervalSinceReferenceDate: 677876053.640059).beginningOfDay,
            Date(year: 2022, month: 06, day: 25)
        )
    }
    
    func testBeginningOfWeek() {
        XCTAssertEqual(testDate?.beginningOfWeek, Date(year: 2022, month: 06, day: 19))
    }
    
    func testEndOfWeek() {
        XCTAssertEqual(testDate?.endOfWeek, Date(year: 2022, month: 06, day: 25, hour: 23, minute: 59, second: 59))
    }
    
    func testTimeZones() {
        //Is there ever an issue if time time zone changes?
        //The API responses for dates is inconsistent, as often they are supplied with YYYY-MM-DD
        //But sometimes they respond with a string describing ISO8601 dates
        
        //As long as the timeZone is consistent between the date formatter and the calendar that produces the dates, this shouldn't be an issue
        let dateComponents = DateComponents(year: 2022, month: 6, day: 22)
        
        var cal = Calendar(identifier: .gregorian)
        for id in TimeZone.knownTimeZoneIdentifiers {
            if let zone = TimeZone(identifier: id) {
                cal.timeZone = zone
                let date = cal.date(from: dateComponents)
                NHLAPI.shortDateFormatter.timeZone = zone
                if NHLAPI.shortDateFormatter.string(from: date!) != "2022-06-22" {
                    XCTFail("Failure for \(zone.identifier) - \(String(describing: zone.abbreviation()))")
                }
            }
        }
    }
    
}
