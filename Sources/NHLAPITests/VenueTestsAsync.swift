//
//  VenueTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class VenueTestsAsync: NHLAPIAsynchronousTestCase {
    
    func testVenuePublisher() {
        testAsynchronously(timeout: 10) { ex in
            asyncCancellable = VenueResult
                .publisher(id: 5046)
                .sink(receiveCompletion: basicSinkCompletion) { venue in
                    XCTAssertNotNil(venue)
                    ex.fulfill()
                }
        }
    }

}
