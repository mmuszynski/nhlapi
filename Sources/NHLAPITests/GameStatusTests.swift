//
//  TeamsTests.swift
//
//
//  Created by Mike Muszynski on 2/15/21.
//

import XCTest
import Combine
@testable import NHLAPI

class GameStatusTest: NHLAPITestCase {
    
    func testDecodeGameStatusResult() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "GameStatusResult")
            let _ = try NHLAPI.decode(Array<GameStatusResult>.self, from: data)
        }
    }
}
