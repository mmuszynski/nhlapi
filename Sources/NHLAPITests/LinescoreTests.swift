//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/16/21.
//

import XCTest
@testable import NHLAPI

class LinescoreTests: NHLAPITestCase {
    func testDecodeLinescore() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "LinescoreInProgress")
            let preview = try jsonDataFromFile(named: "LinescoreUpcomingGame")
            let finished = try jsonDataFromFile(named: "LinescoreFinishedGame")
            let linescore = try NHLAPI.decode(Linescore.self, from: data)
            
            XCTAssertEqual(linescore.homeTeamName, "Chicago Blackhawks")
            XCTAssertEqual(linescore.awayTeamName, "Toronto Maple Leafs")

            let previewLinescore = try NHLAPI.decode(Linescore.self, from: preview)
            XCTAssertFalse(previewLinescore.isInProgress)
            XCTAssertFalse(previewLinescore.isFinished)

            let finishedLinescore = try NHLAPI.decode(Linescore.self, from: finished)
            XCTAssertTrue(finishedLinescore.isFinished)
        }
    }
}
