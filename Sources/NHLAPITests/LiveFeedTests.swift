//
//  LiveFeedTests.swift
//  NHLAPITests
//
//  Created by Mike Muszynski on 11/25/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import XCTest
@testable import NHLAPI

class LiveFeedTests: NHLAPITestCase {
    
    func testDecodeLiveFeedWithPenaltyBox() {
        let data = try! jsonDataFromFile(named: "LiveFeedWithPenaltyBox")
        tryAPIDecode {
            let _ = try NHLAPI.decode(LiveFeedResult.self, from: data)
        }
    }
    
    func testDecodeLiveFeedGoalInfo() {
        let data = try! jsonDataFromFile(named: "LiveFeedCompleted")
        tryAPIDecode {
            let feed = try NHLAPI.decode(LiveFeedResult.self, from: data)
            let goals = feed.plays.filter { $0.playType == .goal }
            XCTAssertEqual(goals.first?.period, 1)
            XCTAssertEqual(goals.first?.playDescription, "Joonas Donskoi (1) Tip-In, assists: Samuel Girard (1), Andre Burakovsky (1)")
        }
    }
    
    func testDecodePenalties() {
        let data = try! jsonDataFromFile(named: "LiveFeedCompleted")
        tryAPIDecode {
            let feed = try NHLAPI.decode(LiveFeedResult.self, from: data)
            let penalties = feed.plays.filter { $0.playType == .penalty }
            let penaltyCount = penalties.map { $0.result.secondaryType }.count
            XCTAssertEqual(penaltyCount, 15)
        }
    }
    
    func testDecodeCompletedLiveFeed() {
        tryAPIDecode {
            let data = try jsonDataFromFile(named: "LiveFeedCompleted")
            let liveFeed = try NHLAPI.decode(LiveFeedResult.self, from: data)
            
            let shotCount = liveFeed.allPlays.filter { $0.playType == .shot || $0.playType == .goal }.count
            let shots = liveFeed.linescore.awayTeamShots + liveFeed.linescore.homeTeamShots
            
            XCTAssertTrue(liveFeed.plays(forPeriod: 0).isEmpty)
            XCTAssertTrue(!liveFeed.plays(forPeriod: 1).isEmpty)
            
            XCTAssertEqual(shotCount, shots)
        }
    }
    
    
}
