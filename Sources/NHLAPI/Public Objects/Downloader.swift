//
//  NHLApiDownloader.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 3/12/18.
//  Copyright © 2018 Mike Muszynski. All rights reserved.
//

import Foundation
import Combine

enum NHLAPIDownloaderError: Error {
    case unknownResponse(_ response: URLResponse)
    case invalidResponse(_ response: HTTPURLResponse)
    case couldNotLoadData
}

var shortDateFormatter: ISO8601DateFormatter {
    let iso = ISO8601DateFormatter()
    iso.formatOptions = [.withYear, .withDay, .withMonth, .withDashSeparatorInDate]
    return iso
}

public class Downloader {
    public init() { }
    
    public static var debug: Bool = false
    public static var urlHistory: [URL] = []
    public func clearHistory() {
        Downloader.urlHistory.removeAll()
    }
    
    public func getData(for endpoint: Endpoint) throws -> Data {
        let url = endpoint.url
        
        //Append to the history
        Downloader.urlHistory.append(url)
        
        let json = try Data(contentsOf: url)
        return json
    }
    
    public func data(for endpoint: Endpoint, expanding expands: [EndpointExpansion] = []) async throws -> Data {
        if Downloader.debug {
            print("Downloading result for endpoint: \(endpoint.url)")
        }
        
        let url = endpoint.url(expanding: expands)
        
        //Append to the history
        Downloader.urlHistory.append(url)
        
        if let cachedData = cachedData(for: url) {
            if Downloader.debug {
                print("returning cached data for \(url)")
            }
            
            return cachedData
        }
        
        let (data, response) = try await URLSession(configuration: .ephemeral).data(from: url)
        
        guard let response = response as? HTTPURLResponse else {
            throw NHLAPIDownloaderError.unknownResponse(response)
        }
        
        guard response.statusCode == 200 else {
            throw NHLAPIDownloaderError.invalidResponse(response)
        }
        
        //Sets the cache for 5 minutes, no matter what.
        //I think I want to change this.
        cache(data, for: url, expires: Date().addingTimeInterval(300))
        
        return data
    }
    
}

extension Downloader {
    static func publisher<T: NHLAPIResult>(for url: URL) -> AnyPublisher<T, Error> {
        if Downloader.debug { print(url) }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: T.self, decoder: NHLAPI.decoder)
            .eraseToAnyPublisher()
    }
}
