//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation

public enum Endpoint {
    case conferences
    case standings
    case teams(ids: [Int] = [])
    case linescore(game: Int)
    
    case schedule(dateRange: ClosedRange<Date>? = nil)
    case weeklySchedule(containing: Date? = nil)
    case seasonSchedule(seasonId: String)
    
    case liveFeed(game: Int)
    case boxscore(id: Int)
    case seasons
    case season(id: String)
    case currentSeason
    case venue(id: Int)
    case venues
    
    private var base: URL {
        var components = URLComponents()
        components.host = "statsapi.web.nhl.com"
        components.path = "/api/v1"
        components.queryItems = []
        components.scheme = "https"
        
        switch self {
        case .standings:
            components.path.append("/standings")
        case .conferences:
            components.path.append("/conferences")
        case .teams(let ids):
            components.path.append("/teams")
            
            let idFragment = ids.reduce("") { (result, int) -> String in
                return result + ",\(int)"
            }.dropFirst()
            let idString = String(idFragment)
            
            components.queryItems?.append(URLQueryItem(name: "teamId", value: idString))
        case .linescore(let id):
            components.path.append("/game/\(id)/linescore")
        case .schedule(let range):
            components.path.append("/schedule")
            
            if let date = range?.lowerBound {
                let dateString = shortDateFormatter.string(from: date)
                components.queryItems?.append(URLQueryItem(name: "startDate", value: dateString))
            }
            
            if let date = range?.upperBound {
                let dateString = shortDateFormatter.string(from: date)
                components.queryItems?.append(URLQueryItem(name: "endDate", value: dateString))
            }
        case .weeklySchedule(let date):
            components.path.append("/schedule")
            let date = date ?? Date()
            
            //get week start
            guard let start = date.beginningOfWeek,
                  let end = date.endOfWeek?.beginningOfDay else {
                fatalError()
            }
            
            return Endpoint.schedule(dateRange: start...end).base
        case .seasonSchedule(let id):
            components.path.append("/schedule")
            components.queryItems?.append(URLQueryItem(name: "seasonId", value: id))
        case .liveFeed(let id):
            components.path.append("/game/\(id)/feed/live")
        case .boxscore(let id):
            components.path.append("/game/\(id)/boxscore")
        case .seasons:
            components.path.append("/seasons")
        case .season(let id):
            components.path.append("/seasons/\(id)")
        case .currentSeason:
            components.path.append("/seasons/current")
        case .venues:
            components.path.append("/venues")
        case .venue(let id):
            components.path.append("/venues/\(id)")
        }
        
        if components.queryItems == [] { components.queryItems = nil }
        return components.url!
    }
    
    public var url: URL {
        return self.url(appending: [], expanding: [])
    }
    
    public func url(appending queryItems: [URLQueryItem] = [], expanding expands: [EndpointExpansion]) -> URL {
        var components = URLComponents(url: self.base, resolvingAgainstBaseURL: false)!
        if components.queryItems == nil {
            components.queryItems = [URLQueryItem]()
        }
        for item in queryItems {
            components.queryItems?.append(item)
        }
        for expand in expands {
            components.queryItems?.append(expand.query)
        }
        if components.queryItems == [] { components.queryItems = nil }
        let finalURL = components.url!
        return finalURL
    }
    
    static func scheduleForDay(containing date: Date) -> Endpoint {
        return .schedule(dateRange: date...date)
    }
}


extension Endpoint: Hashable {}

// Most classes have needed this
extension Endpoint {
    
    public func fetchData(expanding expands: [EndpointExpansion] = []) async throws -> Data {
        let downloader = Downloader()
        let data = try await downloader.data(for: self, expanding: expands)
        return data
    }
    
    public func fetchResult<T: NHLAPIResult>(expanding expands: [EndpointExpansion] = []) async throws -> T {
        let data = try await self.fetchData(expanding: expands)
        let result = try NHLAPI.decoder.decode(T.self, from: data)
        
        //cache here?
        return result
    }
    
}
