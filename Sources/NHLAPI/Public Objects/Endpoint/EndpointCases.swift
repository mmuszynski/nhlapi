//
//  File.swift
//  
//
//  Created by Mike Muszynski on 7/5/22.
//

import Foundation

extension Endpoint {
    public static var commonCases: [Endpoint] {
        [.conferences, .standings, .teams(), .schedule(), .weeklySchedule(), .seasons, .currentSeason, .venues]
    }
    
    public static func customizableCases(with defaults: [Any] = [Array<Int>(), "String", 1, Date(), Date()...Date().addingTimeInterval(1)]) -> [Endpoint] {
        func first<T>() -> T? {
            defaults.first(where: {
                $0 is T
            }) as? T
        }
        
        return [.teams(ids: first() ?? []),
                .linescore(game: first()!),
                .scheduleForDay(containing: first()!),
                .schedule(dateRange: first()),
                .weeklySchedule(containing: first()),
                .seasonSchedule(seasonId: first()!),
                .liveFeed(game: first()!),
                .boxscore(id: first()!),
                .season(id: first()!),
                .venue(id: first()!)]
    }
}
