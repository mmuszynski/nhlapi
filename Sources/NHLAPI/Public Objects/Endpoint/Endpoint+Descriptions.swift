//
//  File.swift
//  
//
//  Created by Mike Muszynski on 7/5/22.
//

import Foundation

fileprivate extension Date {
    var shortString: String {
        NHLAPI.shortDateFormatter.string(from: self)
    }
}

fileprivate extension ClosedRange where Bound == Date {
    var shortString: String {
        let lower = NHLAPI.shortDateFormatter.string(from: self.lowerBound)
        let upper = NHLAPI.shortDateFormatter.string(from: self.upperBound)
        return "\(lower) - \(upper)"
    }
}

extension Endpoint: CustomStringConvertible {
    public var description: String {
        switch self {
        case .conferences:
            return "Conferences"
        case .standings:
            return "Standings"
        case .teams(ids: let ids):
            return "Teams" + (ids.isEmpty ? "" : ids.reduce("", { partialResult, int in
                partialResult + "\(int), "
            })).dropLast(2)
        case .linescore(game: let game):
            return "Linescore for \(game)"
        case .schedule(dateRange: let dateRange):
            if let dateRange {
                if dateRange.lowerBound == dateRange.upperBound {
                    return "Schedule for \(dateRange.lowerBound.shortString)"
                } else {
                    return "Schedule for \(dateRange.shortString)"
                }
            } else {
                return "Today's Schedule"
            }
        case .weeklySchedule(containing: let containing):
            if let week = containing?.beginningOfWeek {
                return "Schedule for Week of \(week.shortString)"
            } else {
                return "This Week's Schedule"
            }
        case .seasonSchedule(seasonId: let seasonId):
            return "Season Schedule \(seasonId)"
        case .liveFeed(game: let game):
            return "Live Feed \(game)"
        case .boxscore(id: let id):
            return "Boxscore \(id)"
        case .seasons:
            return "Seasons"
        case .season(id: let id):
            return "Season \(id)"
        case .currentSeason:
            return "Current Season"
        case .venue(id: let id):
            return "Venue \(id)"
        case .venues:
            return "Venues"
        }
    }
}
