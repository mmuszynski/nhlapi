//
//  File.swift
//  
//
//  Created by Mike Muszynski on 3/29/21.
//

import Foundation

public protocol EndpointExpansion {
    var query: URLQueryItem { get }
}
