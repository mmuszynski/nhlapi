//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/17/21.
//

import Foundation
import Combine

public class Season {
    
    enum FetchError: Error {
        case noSeason
    }
    
    public convenience init(forYear year: Int) async throws {
        let secondYear = year + 1
        let yearID = "\(year)\(secondYear)"
        let downloader = Downloader()
        
        let data = try await downloader.data(for: .season(id: yearID))
        let result = try NHLAPI.decoder.decode(SeasonsResult.self, from: data)
        guard let seasonResult = result.seasons.first else {
            throw FetchError.noSeason
        }
        self.init(seasonResult)
    }
    
    private init(_ seasonAPIResult: SeasonResult) {
        self.regularSeasonStartDate = seasonAPIResult.regularSeasonStartDate
        self.regularSeasonEndDate = seasonAPIResult.regularSeasonEndDate
        self.seasonEndDate = seasonAPIResult.seasonEndDate
        self.id = seasonAPIResult.seasonId
    }
    
    var regularSeasonStartDate: Date
    var regularSeasonEndDate: Date
    var seasonEndDate: Date
    var id: String
    
    /// The entirety of the regular season for the given season
    var fullRegularSeasonSchedule: Schedule {
        get async throws {
            if let _fullRegularSeasonSchedule { return _fullRegularSeasonSchedule }
            let schedule = try await Schedule.forDateRange(regularSeasonStartDate...regularSeasonEndDate)
            self._fullRegularSeasonSchedule = schedule
            return _fullRegularSeasonSchedule!
        }
    }
    private var _fullRegularSeasonSchedule: Schedule?
    
    var fullSeasonSchedule: Schedule {
        get async throws {
            if let _fullRegularSeasonSchedule { return _fullRegularSeasonSchedule }
            let result: ScheduleResult = try await Endpoint.seasonSchedule(seasonId: id).fetchResult()
            return Schedule(result)
        }
    }
    private var _fullSeasonSchedule: Schedule?
    
}

/// Publishers used to get information about a specific season
/// Used when you have a season and want to get associated information about it
extension SeasonResult {
    func publisherForScheduledGames(includingPostseason shouldIncludePostseason: Bool = false) -> AnyPublisher<Array<ScheduleGameResult>, Error> {
        let dateRange = self.regularSeasonStartDate...(shouldIncludePostseason ? self.seasonEndDate : self.regularSeasonEndDate)
        return URLSession.shared
            .dataTaskPublisher(for: Endpoint.schedule(dateRange: dateRange).url)
            .map { $0.data }
            .decode(type: ScheduleResult.self, decoder: NHLAPI.decoder)
            .map { $0.allGames }
            .eraseToAnyPublisher()
    }
}

