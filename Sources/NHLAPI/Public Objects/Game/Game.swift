//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/28/21.
//

import Foundation

//An object representing an NHLAPI Game
public class Game: NHLAPIBaseObject {
    
    /// A description for the status of the game (more info to come)
    public enum Status {
        case upcoming
        case live
        case liveCritical
        case final
        case finalOT
        case unknown(_ flag: String)
        
        init(result: GameStatus) {
            switch result.statusCode {
            case "1":
                self = .upcoming
            case "3":
                self = .live
            case "4":
                self = .liveCritical
            case "6":
                self = .finalOT
            case "7":
                self = .final
            default:
                print("Unknown status code for \(result)")
                self = .unknown(result.statusCode)
            }
        }
    }
    
    public var id: Int
    
    public var homeTeam: Team
    public var awayTeam: Team
    public var homeScore: Int
    public var awayScore: Int
    
    public var date: Date
    public var status: Status
    
    private var _liveFeed: LiveFeed?
    public var liveFeed: LiveFeed {
        get async {
            if let _liveFeed {
                return _liveFeed
            }
            do {
                _liveFeed = try await LiveFeed.forGame(id: self.id)
                return _liveFeed!
            } catch {
                fatalError("\(error)")
            }
        }
    }
        
    init(_ gameResult: ScheduleGameResult) {
        self.homeTeam = Team(gameResult.homeTeam)
        self.awayTeam = Team(gameResult.awayTeam)
        self.homeScore = gameResult.homeScore
        self.awayScore = gameResult.awayScore
        
        self.date = gameResult.gameDate
        self.status = Status(result: gameResult.gameStatusResult)
        
        self.id = gameResult.id
    }
    
}

extension Game.Status: CustomStringConvertible {
    public var description: String {
        
        switch self {
        case .upcoming:
            return "Upcoming"
        case .live:
            return "Live"
        case .liveCritical:
            return "Live - Critical"
        case .final:
            return "Final"
        case .finalOT:
            return "Final (OT)"
        case .unknown(let status):
            return "Unknown " + status
        }
    }
}
