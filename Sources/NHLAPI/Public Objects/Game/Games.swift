//
//  File.swift
//  
//
//  Created by Mike Muszynski on 6/21/22.
//

import Foundation

//An object representing a list of NHLAPI Games
public class Games: NHLAPIBaseObject, SimpleCollection {
    public typealias Element = Game
    public var elements: [Element] = []
    
    /// Initialize with an array of `Game` objects
    /// - Parameter games: The games that the collection holds
    public init(_ games: [Element] = []) {
        super.init()
        self.elements = games
    }
    
    init(_ result: ScheduleResult) {
        self.elements = result.allGames.map { Game($0) }
        super.init()
        self.copyright = result.copyright
    }
    
    init(_ result: [ScheduleGameResult]) {
        self.elements = result.map { Game($0) }
        super.init()
    }
    
    /// All of today's games
    public static var today: Games {
        get async throws {
            let downloader = Downloader()
            let data = try await downloader.data(for: .schedule())
            let schedule = try NHLAPI.decoder.decode(ScheduleResult.self, from: data)
            
            return Games(schedule)
        }
    }
    
    /// Returns games for yesterday
    public static var yesterday: Games {
        get async throws {
            let negativeOneDay = DateComponents(day: -1)
            let yesterday = Calendar.current.date(byAdding: negativeOneDay, to: Date())!
            return try await Games.forDay(containing: yesterday)
        }
    }
    
    /// All games for a certain date
    public static func forDay(containing date: Date, expanding expands: [EndpointExpansion] = []) async throws -> Games {
        let schedule: ScheduleResult = try await Endpoint.scheduleForDay(containing: date).fetchResult(expanding: expands)
        return Games(schedule)
    }
}

extension Games: RandomAccessCollection {
    public typealias Index = Array<Element>.Index
    public func index(after i: Array<Element>.Index) -> Array<Element>.Index {
        return elements.index(after: i)
    }
    public func index(before i: Array<Element>.Index) -> Array<Element>.Index {
        return elements.index(before: i)
    }
}
