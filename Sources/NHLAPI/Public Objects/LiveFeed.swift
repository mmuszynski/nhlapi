//
//  File.swift
//  
//
//  Created by Mike Muszynski on 1/14/23.
//

import Foundation

public class LiveFeed {
    var result: LiveFeedResult
    
    init(_ result: LiveFeedResult) {
        self.result = result
    }
    
    public static func forGame(id: Int, expanding expands: [EndpointExpansion] = []) async throws -> LiveFeed {
        return try await LiveFeed(Endpoint.liveFeed(game: id).fetchResult(expanding: expands))
    }
}
