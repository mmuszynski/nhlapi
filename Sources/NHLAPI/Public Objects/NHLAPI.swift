//
//  NHL.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 8/28/20.
//  Copyright © 2020 Mike Muszynski. All rights reserved.
//

import Foundation

public class NHLAPI {
    
    public enum ImplementationError: Error {
        case unimplemented
    }
    
    class var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom({ (decoder) -> Date in
            let container = try decoder.singleValueContainer()
            let string = try container.decode(String.self)
            guard let date = NHLAPI.date(from: string) else {
                throw DecodingError.valueNotFound(Date.self, DecodingError.Context(codingPath: container.codingPath, debugDescription: "Could not decode either a short ISO8601 date or a long ISO8601 date for the given data"))
            }
            
            return date
        })
        return decoder
    }
    
    public class func decode<T: Decodable>(from data: Data) throws -> T {
        return try self.decoder.decode(T.self, from: data)
    }
    
    public class func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T {
        return try self.decoder.decode(T.self, from: data)
    }
}
