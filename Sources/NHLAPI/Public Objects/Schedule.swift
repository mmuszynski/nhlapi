//
//  File.swift
//  
//
//  Created by Mike Muszynski on 6/22/22.
//

import Foundation

/// A collection that behaves like a dictionary, keyed by a `Date`, each containing a `Game` object.
///
/// For stability with the NHLAPI, dates provided can be any `Date` object, but it will be "erased" to the beginning of the day in order to provide an equality comparison and serve as a key for the collection.
///
/// Several asynchronous static methods are available to load data, and these will also throw errors if they are not able to
public class Schedule {
    
    /// The elements of the collection, a Dictionary to store the information
    private var elements = Dictionary<Date, Games>()
    
    /// Initializes the collection with the results of a call to the NHLAPI schedule endpoint
    /// - Parameter result: The `ScheduleResult` returned by teh NHLAPI
    init(_ result: ScheduleResult) {
        for day in result.dates {
            self.elements[day.date] = Games(day.games)
        }
    }
    
    /// Returns a collection of `Games` that occur on a given date
    ///
    /// A schedule consists of a collection of games grouped by date. In order to access the games for a given date, that date must be  equal to the one stored in the `Schedule` object. As such, it is "erased" to the beginning of the day. See the method `beginningOfDay` extending the foundation class `Date` for more information.
    public subscript(_ date: Dictionary<Date, Games>.Key) -> Games? {
        let beginningOfDay = date.beginningOfDay!
        return elements[beginningOfDay]
    }
    
    /// Returns a collection of `Games` that occur on a given date, described by a date string
    ///
    /// This method is an alternate strategy of invoking `subscript(_ date:)`, and will only accept a date in either ISO8601 short or long format.
    public subscript(_ dateString: String) -> Games? {
        guard let date = NHLAPI.date(from: dateString) else {
            return nil
        }
        
        return self[date]
    }
    
    /// Iterates over the `Games` objects contained in the collection, collecting their counts and returning the total count for the `Schedule`
    public var gameCount: Int {
        elements.reduce(0) { (part, element) in
            let (_, value) = element
            return part + value.count
        }
    }
    
    public var allGames: Games {
        var games: [Games.Element] = []
        elements.keys.forEach { date in
            if let dayGames = elements[date] {
                games.append(contentsOf: dayGames)
            }
        }
        return Games(games.sorted(by: { $0.date < $1.date }))
    }
    
    /*
     - MARK: Data load methods
     ==========================================================================================
     Static Methods: Methods for creating schedules based on NHLAPI data
     ==========================================================================================
     */
    
    /// Returns the schedule specifically for the current week
    public static var thisWeek: Schedule {
        get async throws {
            return try await Schedule.forWeek()
        }
    }
    
    /// Fetches the schedule for the week that contains the date provided
    /// - Parameters:
    ///   - date: A `Date` which is contained by the range of its week
    ///   - expands: The `EndpointExpansion` objects
    /// - Returns: A `Schedule` for the week that contains the date provided
    public static func forWeek(containing date: Date = Date(), expanding expands: [EndpointExpansion] = []) async throws -> Schedule {
        let range = date.beginningOfWeek!...date.endOfWeek!.beginningOfDay!
        return try await Schedule.forDateRange(range, expanding: expands)
    }
    
    /// Returns the schedule for the current day
    public static var today: Schedule {
        get async throws {
            let endpoint = Endpoint.scheduleForDay(containing: .today)
            return Schedule(try await endpoint.fetchResult())
        }
    }
    
    /// Returns the schedule for the previous day
    public static var yesterday: Schedule {
        get async throws {
            let endpoint = Endpoint.scheduleForDay(containing: .yesterday)
            return Schedule(try await endpoint.fetchResult())
        }
    }
    /// Fetches the schedule for the day that contains the date provided
    /// - Parameter date: The `Date` to download games from
    
    /// Fetches the schedule for the day that contains the date provided
    /// - Parameters:
    ///   - date: The `Date` to download games from
    ///   - expands: The `EndpointExpansion` objects
    /// - Returns: A `Schedule` for the day that contains the date provided
    public static func forDay(containing date: Date, expanding expands: [EndpointExpansion] = []) async throws -> Schedule {
        let endpoint = Endpoint.scheduleForDay(containing: date)
        return Schedule(try await endpoint.fetchResult(expanding: expands))
    }
    
    /// Fetches the schedule for a specific date range provided
    /// - Parameter range: A `ClosedRange<Date>` for the dates required
    /// - Returns: A `Schedule` for the days starting with the `beginningOfDay` of the lower bound of the range and ending with the `beginningOfDay` for the upper bound of the range
    public static func forDateRange(_ range: ClosedRange<Date>, expanding expands: [EndpointExpansion] = []) async throws -> Schedule {
        let endpoint = Endpoint.schedule(dateRange: range)
        return Schedule(try await endpoint.fetchResult(expanding: expands))
    }
    
    /*
     - MARK: Example Data
     ==========================================================================================
     Examples pulled from the NHLAPI for use in tests or SwiftUI preview content
     ==========================================================================================
     */
    
    public static var example: Schedule {
        Schedule(try! .example)
    }
    
    public static var exampleWithTeamExpand: Games {
        Games(try! ScheduleResult.exampleWithTeamExpand)
    }
    
    public static var exampleWithLinescoreExpand: Games {
        Games(try! ScheduleResult.exampleWithLinescoreExpand)
    }
}
