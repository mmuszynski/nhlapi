//
//  PreviewContent.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/7/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

private class PreviewDummy {}

extension ScheduleGameResult {
    public static var testGames: [ScheduleGameResult] {
        let now = Date(timeIntervalSinceReferenceDate: 595025715.386615)
                
        let url = Bundle(for: PreviewDummy.self).url(forResource: "ScheduleTestData", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        let schedule = try! NHLAPI.decode(ScheduleResult.self, from: data)
        let todaysGames = schedule[now]

        return todaysGames!.games
    }
    
    public static var inProgress: ScheduleGameResult {
        return ScheduleGameResult.testGames[0]
    }
    
    public static var upcoming: ScheduleGameResult {
        return ScheduleGameResult.testGames[1]
    }
}

extension Linescore {
    public static var gameInProgress: Linescore {
        let url = Bundle.main.url(forResource: "LinescoreTestData", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        return try! NHLAPI.decode(Linescore.self, from: data)
    }
    
    public static var upcoming: Linescore {
        let url = Bundle.main.url(forResource: "UpcomingGameLinescore", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        return try! NHLAPI.decode(Linescore.self, from: data)
    }
    
    public static var final: Linescore {
        let url = Bundle.main.url(forResource: "FinishedGameLinescore", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        return try! NHLAPI.decode(Linescore.self, from: data)
    }
}

extension LiveFeedResult {
    public static var completed: LiveFeedResult {
        let url = Bundle.main.url(forResource: "LiveFeedCompleted", withExtension: "json")!
        let data = try! Data(contentsOf: url)
        let feed = try! NHLAPI.decode(LiveFeedResult.self, from: data)
        
        return feed
    }
}
