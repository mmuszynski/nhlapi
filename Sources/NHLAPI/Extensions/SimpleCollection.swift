//
//  SimpleCollection.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public protocol SimpleCollection<Element>: Collection where Index == Array<Element>.Index {
    associatedtype Element
    var elements: [Element] { get }
}

extension SimpleCollection {
    public var startIndex: Index { return elements.startIndex }
    public var endIndex: Index { return elements.endIndex }

    public subscript(index: Index) -> Iterator.Element {
        get { return elements[index] }
    }

    public func index(after i: Index) -> Index {
        return elements.index(after: i)
    }
}
