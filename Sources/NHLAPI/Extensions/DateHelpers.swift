//
//  File.swift
//  
//
//  Created by Mike Muszynski on 6/25/22.
//

import Foundation

extension Array where Element == Calendar.Component {
    func date(from date: Date) -> Date? {
        let comps = Calendar.current.dateComponents(Set(self), from: date)
        return Calendar.current.date(from: comps)
    }
}

extension Date {
    var beginningOfDay: Date? {
        [Calendar.Component.year, .month, .day].date(from: self)
    }
    
    var beginningOfWeek: Date? {
        [Calendar.Component.weekOfYear, .yearForWeekOfYear].date(from: self)
    }
    
    var beginningOfMonth: Date? {
        let comps = Calendar.current.dateComponents([.calendar, .month, .year], from: self)
        return Calendar.current.date(from: comps)
    }
    
    var endOfWeek: Date? {
        var comps = Calendar.current.dateComponents([.calendar, .weekOfYear , .yearForWeekOfYear], from: self)
        comps.weekOfYear = comps.weekOfYear! + 1
        comps.second = -1
        return Calendar.current.date(from: comps)
    }
    
    static var today: Date {
        return Date()
    }
    
    static var yesterday: Date {
        let negativeOneDay = DateComponents(day: -1)
        return Calendar.current.date(byAdding: negativeOneDay, to: .today)!
    }
}

extension DateComponents {
    static let oneDay = DateComponents(day: 1)
}


extension NHLAPI {
    static var shortDateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        //formatter.timeZone = Calendar.current.timeZone
        formatter.formatOptions = [.withYear, .withMonth, .withDay, .withDashSeparatorInDate]
        return formatter
    }()
    
    static var longDateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        //formatter.timeZone = Calendar.current.timeZone
        return formatter
    }()
    
    public static func date(from string: String) -> Date? {
        if let date = NHLAPI.longDateFormatter.date(from: string) {
            return date
        } else if let date = NHLAPI.shortDateFormatter.date(from: string) {
            return date
        }
        return nil
    }
}
