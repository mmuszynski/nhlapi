//
//  File.swift
//  
//
//  Created by Mike Muszynski on 6/26/22.
//

import Foundation

extension String {
    static let cacheValuePath = "com.mmuszynski.NHLAPI.cachedValue"
    static let cacheExpirationPath = "com.mmuszynski.NHLAPI.cachedValueExpiration"
}

extension Downloader {
    
    static let cache = UserDefaults()
    
    func cache(_ result: Data, for url: URL, expires expiration: Date) {
        if Downloader.debug {
            print("Writing cached value for \(url.absoluteString), expiration: \(expiration)")
        }
        
        Downloader.cache.set(result, forKey: .cacheValuePath.appending(".\(url.absoluteString)"))
        Downloader.cache.set(expiration, forKey: .cacheExpirationPath.appending(".\(url.absoluteString)"))
    }
    
    func cachedData(for url: URL) -> Data? {
        let cachePath: String = .cacheValuePath.appending(".\(url.absoluteString)")
        let cacheExpiryPath: String = .cacheExpirationPath.appending(".\(url.absoluteString)")

        if Downloader.debug {
            print("Checking cached data for \(url.absoluteString)")
        }
        
        guard let date = Downloader.cache.object(forKey: cacheExpiryPath) as? Date else {
            if Downloader.debug {
                print("No expiration found for \(url.absoluteString)")
            }
            return nil
        }
                
        guard date.compare(Date()) == .orderedDescending else {
            if Downloader.debug {
                print("Expired data found for \(url.absoluteString)")
            }
            Downloader.cache.set(nil, forKey: cacheExpiryPath)
            return nil
        }
        
        return Downloader.cache.data(forKey: cachePath)
    }
    
    func purgeCache() {
        let domain = Bundle.main.bundleIdentifier
        Downloader.cache.removePersistentDomain(forName: domain!)
        Downloader.cache.synchronize()
    }
    
}
