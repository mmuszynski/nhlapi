//
//  File.swift
//  
//
//  Created by Mike Muszynski on 6/26/22.
//

import Foundation

public protocol NHLAPIResult: Codable {
    var wait: Int? { get }
}
