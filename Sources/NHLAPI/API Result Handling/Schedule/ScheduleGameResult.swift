//
//  ScheduleGameResult.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 11/8/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let shortTime: DateFormatter = {
        let df =  DateFormatter()
        df.dateFormat = "h:mm a"
        return df
    }()
}

public struct ScheduleGameResult: Codable {
    var gamePk: Int
    var link: String
    var gameType: String
    var season: String
    var gameDate: Date
    public var linescore: Linescore?
    
    var gameStatusResult: GameStatus
    var scheduleGameTeamsResult: [String: ScheduleGameTeamResult]
    var gameVenueResult: GameVenueResult
    var contentResult: NHLContent
    
    enum CodingKeys: String, CodingKey {
        case gamePk
        case link
        case gameType
        case season
        case gameDate
        case linescore
        
        case gameStatusResult = "status"
        case scheduleGameTeamsResult = "teams"
        case gameVenueResult = "venue"
        case contentResult = "content"
    }
    
    public var id: Int {
        return self.gamePk
    }
    
    /// Result for the home team returned by a call to the game schedule Endpoint
    ///
    /// Includes record, score, and team information.
    /// - note: Record information appears to be somewhat unreliable
    private var homeTeamResult: ScheduleGameTeamResult { self.scheduleGameTeamsResult["home"]! }
    
    /// Result for the away team returned by a call to the game schedule Endpoint
    ///
    /// Includes record, score, and team information.
    /// - note: Record information appears to be somewhat unreliable
    private var awayTeamResult: ScheduleGameTeamResult { self.scheduleGameTeamsResult["away"]! }
    
    /// Team information result for the home team returned by the game schedule Endpoint
    ///
    /// The schedule `Endpoint` returns very limited information about a team by default. By applying "schedule.teams" to the endpoint's expand variable, it becomes possible to return a full `TeamResult` object
    var homeTeam: TeamResult { self.homeTeamResult.team }
    
    /// Team information result for the away team returned by the game schedule Endpoint
    ///
    /// The schedule `Endpoint` returns very limited information about a team by default. By applying "schedule.teams" to the endpoint's expand variable, it becomes possible to return a full `TeamResult` object
    var awayTeam: TeamResult { self.awayTeamResult.team }
        
    public var isUpcoming: Bool {
        return ["1", "2"].contains(self.gameStatusResult.codedGameState)
    }
    
    public var isFinal: Bool {
        return self.gameStatusResult.abstractGameState == "Final"
    }
    
    public var venueName: String {
        return self.gameVenueResult.name
    }
    
    public var homeTeamID: Int {
        return self.homeTeam.id
    }

    public var awayTeamID: Int {
        return self.awayTeam.id
    }
    
    public var homeRecord: ScheduleGameTeamRecord {
        return self.homeTeamResult.leagueRecord
    }
    
    public var awayRecord: ScheduleGameTeamRecord {
        return self.awayTeamResult.leagueRecord
    }
    
    /// The number of goals the home team has scored
    public var homeScore: Int {
        self.homeTeamResult.score
    }
    
    /// The number of goals the away team has scored
    public var awayScore: Int {
        self.awayTeamResult.score
    }
}

extension ScheduleGameResult: Hashable {
    public static func == (lhs: ScheduleGameResult, rhs: ScheduleGameResult) -> Bool {
        return lhs.gamePk == rhs.gamePk
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(gamePk)
    }
}
