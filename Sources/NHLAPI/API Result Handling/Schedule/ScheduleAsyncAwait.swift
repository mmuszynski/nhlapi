//
//  File.swift
//  
//
//  Created by Mike Muszynski on 6/9/21.
//

import Foundation

@available(iOS 15.0, *)
@available(macOS 12.0, *)
extension ScheduleResult {
    static var today: ScheduleResult {
        get async throws {
            let endpoint = Endpoint.schedule()
            let (data, response) = try await URLSession.shared.data(from: endpoint.url)
            guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError() }
            let result = try NHLAPI.decoder.decode(Self.self, from: data)
            return result
        }
    }
}
