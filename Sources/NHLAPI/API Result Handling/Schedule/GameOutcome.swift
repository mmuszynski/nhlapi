//
//  File.swift
//  
//
//  Created by Mike Muszynski on 3/26/21.
//

import Foundation

extension NHLAPI {
    enum GameOutcome {
        case win
        case loss
        case draw
        case otwin
        case otloss
        case sowin
        case soloss
        case undecided
    }
}

extension ScheduleGameResult {
    private func outcome(for teamID: Int) -> NHLAPI.GameOutcome {
        guard self.isFinal else {
            return .undecided
        }
        
        if self.awayScore > self.homeScore {
            return teamID == self.awayTeamID ? .win : .loss
        } else if self.homeScore > self.awayScore {
            return teamID == self.homeTeamID ? .win : .loss
        } else {
            return .draw
        }
    }
    
    var homeTeamOutcome: NHLAPI.GameOutcome {
        return outcome(for: self.homeTeamID)
    }
    
    var awayTeamOutcome: NHLAPI.GameOutcome {
        return outcome(for: self.awayTeamID)
    }
}
