//
//  ScheduleDateResult.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 11/8/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct ScheduleDateResult: Codable {
    @available(*, unavailable, renamed: "NHLAPI.shortDateFormatter")
    static var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "YY-mm-dd"
        return formatter
    }
    
    public var dateString: String
    var date: Date {
        return NHLAPI.shortDateFormatter.date(from: self.dateString)!
    }
    
    var totalItems: Int
    var totalEvents: Int
    var totalGames: Int
    var totalMatches: Int
    
    public var games: [ScheduleGameResult]
    
    enum CodingKeys: String, CodingKey {
        case dateString = "date"
        case totalItems
        case totalEvents
        case totalGames
        case totalMatches
        case games
    }
}

extension ScheduleDateResult: SimpleCollection {
    public typealias Element = ScheduleGameResult
    public var elements: [Element] {
        return games
    }
}
