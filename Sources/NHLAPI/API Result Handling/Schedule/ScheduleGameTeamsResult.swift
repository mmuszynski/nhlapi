//
//  ScheduleGameTeamsResult.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 11/8/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct ScheduleGameTeamResult: Codable {
    var leagueRecord: ScheduleGameTeamRecord
    var score: Int
    var team: TeamResult
}

public struct ScheduleGameTeamRecord: Codable {
    public var wins: Int
    public var losses: Int
    public var ties: Int?
    public var ot: Int?
    public var type: String
}
