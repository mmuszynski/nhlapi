//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation
import Combine

extension ScheduleResult {
    public enum Expansion: String, EndpointExpansion {
        case linescore = "schedule.linescore"
        case scoringPlays = "schedule.scoringplays"
        case decisions = "schedule.decisions"
        case teams = "schedule.teams"
        case tickets = "schedule.ticket"
        case venue = "schedule.venue"
        case broadcasts = "schedule.broadcasts"
        case allBroadcasts = "schedule.broadcasts.all"
        case radioBroadcasts = "schedule.radioBroadcasts"
        case metadata = "schedule.metadata"
        case contentAll = "schedule.game.content.all"
        case mediaAll = "schedule.game.content.media.all"
        case editorialAll = "schedule.game.content.editorial.all"
        case editorialPreview = "schedule.game.content.editorial.preview"
        case editorialRecap = "schedule.game.content.editorial.recap"
        case editorialArticles = "schedule.game.content.editorial.articles"
        case mediaEPG = "schedule.game.content.media.epg"
        case milestones = "schedule.game.content.media.milestones"
        case highlightsAll = "schedule.game.content.highlights.all"
        case highlightsScoreboard = "schedule.game.content.highlights.scoreboard"
        case highlightsGamecenter = "schedule.game.content.highlights.gamecenter"
        case highlightsMilestone = "schedule.game.content.highlights.milestone"
        case seriesSummary = "schedule.game.seriesSummary"
        
        public var query: URLQueryItem { URLQueryItem(name: "expand", value: rawValue) }
    }
}

extension ScheduleGameResult {
    public typealias PublisherType = AnyPublisher<Array<ScheduleGameResult>, Error>

    /// A publisher that will retrieve an array of `ScheduleGameResult` objects for a given `Date`.
    ///
    /// Creates a `Publisher` that will retrieve an array of `ScheduleGameResult` objects for a given `Date`. Does not specify which thread to receive on.
    ///
    /// - Warning: You must specify `.receive(on: Scheduler)` in order to successfully download data.
    /// - Parameter gameID: The ID of the game to be fetched
    /// - Parameter expansions: `The ScheduleResult.Expansions` used to request more API data
    static func publisher(for date: Date = Date(), expanding expansions: [ScheduleResult.Expansion]  = []) -> PublisherType {
        let url = Endpoint.schedule(dateRange: date...date).url(expanding: expansions)
        return Self.publisher(for: url)
    }
    
    static func publisher(for dateRange: ClosedRange<Date>, expanding expansions: [ScheduleResult.Expansion]  = []) -> PublisherType {
        let url = Endpoint.schedule(dateRange: dateRange).url(expanding: expansions)
        return Self.publisher(for: url)
    }
    
    @available(*, deprecated, message: "Range<Date> based Schedule API calls provide inconsistent results and should be avoided")
    static func publisher(forTeam teamId: Int, dateRange: ClosedRange<Date>, expanding expansions: [ScheduleResult.Expansion]  = []) -> PublisherType {
        let url = Endpoint.schedule(dateRange: dateRange).url(appending: [URLQueryItem(name: "teamId", value: "\(teamId)")], expanding: expansions)
        return Self.publisher(for: url)
    }
    
    private static func publisher(for url: URL) -> PublisherType {
        let schedulePublisher: AnyPublisher<ScheduleResult, Error> = Downloader.publisher(for: url)
        return schedulePublisher
            .map { $0.allGames }
            .eraseToAnyPublisher()
    }
}
