//
//  ScheduleResult.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 11/8/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct ScheduleResult: NHLAPIResult {
    var copyright: String?
    var totalItems: Int
    var totalEvents: Int
    var totalGames: Int
    var totalMatches: Int
    var wait: Int?
    
    public var dates: [ScheduleDateResult]
    
    public var allGames: [ScheduleGameResult] {
        return dates.reduce(Array<ScheduleGameResult>()) { (result, dateResult) -> [ScheduleGameResult] in
            return result + dateResult.games
        }
    }
}

extension ScheduleResult {
    subscript(index: Date) -> ScheduleDateResult? {
        get {
            return dates.first(where: { index > $0.date }) ?? nil
        }
    }
}

// Example data
extension ScheduleResult {
    static var example: ScheduleResult {
        get throws {
            let json = Bundle.module.url(forResource: "ScheduleTestData", withExtension: "json")!
            let data = try Data(contentsOf: json)
            let result = try NHLAPI.decoder.decode(Self.self, from: data)
            return result
        }
    }
    
    static var exampleWithTeamExpand: ScheduleResult {
        get throws {
            let json = Bundle.module.url(forResource: "20220622_ScheduleExpandedWithTeams", withExtension: "json")!
            let data = try Data(contentsOf: json)
            let result = try NHLAPI.decoder.decode(Self.self, from: data)
            return result
        }
    }
    
    static var exampleWithLinescoreExpand: ScheduleResult {
        get throws {
            let json = Bundle.module.url(forResource: "20220622_ScheduleExpandedWithTeams", withExtension: "json")!
            let data = try Data(contentsOf: json)
            let result = try NHLAPI.decoder.decode(Self.self, from: data)
            return result
        }
    }
}
