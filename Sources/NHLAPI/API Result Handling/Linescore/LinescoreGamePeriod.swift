//
//  LinescoreGamePeriod.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/3/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct LinescoreGamePeriod: Codable {
    var periodType: String
    var startTime: String?
    var endTime: String?
    public var num: Int
    var ordinalNum: String
    
    var home: LinescoreGamePeriodStats
    var away: LinescoreGamePeriodStats
    
    public var homeGoals: Int { return self.home.goals }
    public var awayGoals: Int { return self.away.goals }
    public var homeShots: Int { return self.home.shotsOnGoal }
    public var awayShots: Int { return self.away.shotsOnGoal }
}

struct LinescoreGamePeriodStats: Codable {
    var goals: Int
    var shotsOnGoal: Int
    var rinkSide: String?
}

struct LinescoreShootoutInfo: Codable {
    var home: LinescoreTeamShootoutStats
    var away: LinescoreTeamShootoutStats
    var startTime: String?
}

struct LinescoreTeamShootoutStats: Codable {
    var scores: Int
    var attempts: Int
}

struct LinescoreTeamStats: Codable {
    var team: TeamResult
    var goals: Int
    var shotsOnGoal: Int
    var goaliePulled: Bool
    var numSkaters: Int
    var powerPlay: Bool
    
    var teamName: String {
        return team.name
    }
    
    var teamShortName: String? {
        return team.shortName
    }
}

struct LinescoreIntermissionInfo: Codable {
    var intermissionTimeRemaining: Int
    var intermissionTimeElapsed: Int
    var inIntermission: Bool
}

struct LinescorePowerPlayInfo: Codable {
    var situationTimeRemaining: Int
    var situationTimeElapsed: Int
    var inSituation: Bool
}
