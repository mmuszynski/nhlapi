//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation
import Combine

extension Linescore {
    public typealias PublisherType = AnyPublisher<Linescore, Error>

    /// A publisher that will retrieve a `Linescore`.
    ///
    /// Creates a `Publisher` that will retrieve a single `Linescore`. Does not specify which thread to receive on.
    ///
    /// - Warning: You must specify `.receive(on: Scheduler)` in order to successfully download data.
    /// - Parameter gameID: The ID of the game to be fetched
    public static func publisher(for gameID: Int) -> PublisherType {
        return URLSession.shared.dataTaskPublisher(for: Endpoint.linescore(game: gameID).url)
           .map { $0.data }
           .receive(on: DispatchQueue.main)
           .decode(type: Linescore.self, decoder: NHLAPI.decoder)
           .eraseToAnyPublisher()
    }
}

