//
//  LinescoreResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct Linescore: Codable {
    var copyright: String?
    
    /// The current period for the game or 0 if the game is not yet in progress.
    public var currentPeriod: Int
    
    /// A string description for the period (e.g. 1st, 2nd, 3rd, OT, SO). Not present if the game is not yet in progress.
    public var currentPeriodOrdinal: String?
    
    /// The time remaining in the game. Not present if the game is not yet in progress.
    public var currentPeriodTimeRemaining: String?
    
    /// A breakdown of the linescore by period. Empty if the game is not yet in progress.
    var periods: [LinescoreGamePeriod]
    
    /// A description for shootout statistics.
    var shootoutInfo: LinescoreShootoutInfo
    
    /// The teams for the game, with some team information, but not the full `Team` object. See `LinescoreTeamsResult` for further description.
    var teams: [String:LinescoreTeamStats]
    
    /// A description of the current power play status. Not present if the game is not yet in progress.
    var powerPlayStrength: String?
    
    /// Whether or not the game has a shootout.
    public var hasShootout: Bool
    
    /// Information about the intermission. See `LinescoreIntermissionInfo` for further description.
    var intermissionInfo: LinescoreIntermissionInfo
    
    /// Information about the current power play. See `LinescorePowerPlayInfo` for further description.
    var powerPlayInfo: LinescorePowerPlayInfo?
    
    var homeTeam: LinescoreTeamStats {
        return teams["home"]!
    }
    var awayTeam: LinescoreTeamStats {
        return teams["away"]!
    }
    
    /// The full name of the home team.
    public var homeTeamName: String {
        return homeTeam.teamName
    }
    
    public var homeTeamID: Int {
        return homeTeam.team.id
    }
    
    public var awayTeamID: Int {
        return awayTeam.team.id
    }
    
    /// The full name of the away team.
    public var awayTeamName: String {
        return awayTeam.teamName
    }
    
    /// The number of goals the home team has scored.
    public var homeTeamGoals: Int {
        return homeTeam.goals
    }
    
    /// The number of goals the away team has scored.
    public var awayTeamGoals: Int {
        return awayTeam.goals
    }
    
    public var homeTeamShots: Int {
        return homeTeam.shotsOnGoal
    }
    
    public var awayTeamShots: Int {
        return awayTeam.shotsOnGoal
    }
    
    public var isUpcoming: Bool {
        return currentPeriod == 0
    }
    
    @available(*, unavailable, renamed: "isInProgress")
    public var inProgress: Bool {
        fatalError()
    }
    
    public var isInProgress: Bool {
        return !self.isUpcoming && !self.isFinished
    }
    
    public var isFinished: Bool {
        if currentPeriod > 2 && currentPeriodTimeRemaining == "Final" {
            return homeTeamGoals != awayTeamGoals
        }
        return currentPeriod == 5
    }
    
    public var hasOT: Bool {
        return self.periods.count > 3
    }
    
    public func shots(forPeriod period: Int) -> (home: Int, away: Int)? {
        guard let period = self.periods.first(where: { $0.num == period }) else { return nil }
        return (home: period.home.shotsOnGoal, away: period.away.shotsOnGoal)
    }
    
    public func periodInfo(index period: Int) -> LinescoreGamePeriod? {
        guard let period = self.periods.first(where: { $0.num == period }) else { return nil }
        return period
    }
    
    public var homeGoaliePulled: Bool {
        return self.homeTeam.goaliePulled
    }
    
    public var awayGoaliePulled: Bool {
        return self.awayTeam.goaliePulled
    }
    
    public var powerPlay: Bool {
        guard let pps = self.powerPlayStrength else { return false }
        return pps != "Even"
    }
}
