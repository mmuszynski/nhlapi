//
//  File.swift
//
//
//  Created by Mike Muszynski on 6/21/22.
//

import Foundation

//An object representing a list of NHLAPI Teams
public class Teams: NHLAPIBaseObject, SimpleCollection {
    public typealias Element = Team
    public var elements: [Element] = []
    
    /// Initialize with an array of `Game` objects
    /// - Parameter Teams: The Teams that the collection holds
    init(_ Teams: [Element]) {
        super.init()
        self.elements = Teams
    }
    
    init(_ apiResult: TeamsResult) {
        super.init()
        self.elements = apiResult.teams.map { Team($0) }
    }
    
    static func fetchTeamsWith(ids: [Int]) async throws -> Teams {
        let downloader = Downloader()
        let data = try await downloader.data(for: .teams(ids: ids))
        let result = try NHLAPI.decoder.decode(TeamsResult.self, from: data)
                    
        return Teams(result)
    }
    
    static func fetchTeamWith(id: Int) async throws -> Team {
        let teams = try await Teams.fetchTeamsWith(ids: [id])
        guard teams.count == 1 else {
            fatalError("Needs to throw an error")
        }
        return teams.first!
    }
}
