//
//  Publishers.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/9/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation
import Combine

extension TeamResult {
    
    @available(*, unavailable, renamed: "publisher(ids:)")
    public static func publisher(for teamIDs: [Int]) -> AnyPublisher<Array<TeamResult>, Error> {
        fatalError()
    }
    
    /// A `Publisher` that returns a list of `Team`s with the given IDs
    /// - Parameter teamIDs: An array of integers representing the teams to be returned
    /// - Returns: A `Publihser` that returns an `Array` of `Team` objects on completion
    public static func publisher(ids teamIDs: [Int]) -> AnyPublisher<Array<TeamResult>, Error> {
        return URLSession.shared.dataTaskPublisher(for: Endpoint.teams(ids: teamIDs).url)
            .map { $0.data }
            .decode(type: TeamsResult.self, decoder: NHLAPI.decoder)
            .map { $0.teams }
            .eraseToAnyPublisher()
    }
}
