//
//  TeamsResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

/// The API Result that is returned by a call to the Teams endpoint.
///
/// Contains the copyright string and an array of teams
public struct TeamsResult: Codable {
    /// The NHL copyright message
    var copyright: String?
    
    /// A list of teams returned by the API
    var teams: [TeamResult]
}

extension TeamsResult: SimpleCollection {
    public var elements: [TeamResult] {
        return teams
    }
}

