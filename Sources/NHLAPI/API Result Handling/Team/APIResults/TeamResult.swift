//
//  Team.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

/// A representation of an NHL `Team` returned by the `TeamResult` API.
public struct TeamResult: Codable {
    
    /// The unique id number given to the team by the NHL
    public var id: Int
    
    /// The name of the team
    ///
    /// This appears to return a full name including the city and mascot name
    public var name: String
    
    /// A `String` describing the path to the API endpoint for this particular `Team`
    public var link: String
    
    /// An official abbreviation for the `Team`
    ///
    /// May be optional if not all `Team`s return an abbreviation. (e.g. 
    public var abbreviation: String?
    
    public var teamName: String?
    public var locationName: String?
    public var firstYearOfPlay: String?
    public var shortName: String?
    public var officialSiteUrl: String?
    public var franchiseId: Int?
    public var active: Bool?
    
    var venue: VenueResult?
    var division: DivisionResult?
    var conference: ConferenceResult?
    var franchise: FranchiseResult?
    
    var logoURL: URL {
        return URL(string: "https://www-league.nhlstatic.com/images/logos/teams-current-primary-light/\(id).svg")!
    }
}
