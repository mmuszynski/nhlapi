//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/12/23.
//

import Foundation

extension Team: CustomDebugStringConvertible {
    public var debugDescription: String {
        Mirror(reflecting: self).children.reduce("NHLAPI.Team\r") { (partialResult, arg1) in
            let (label, value) = arg1
            if let label {
                return partialResult + label + ": \(value)\r"
            }
            return partialResult
        }
    }
}
