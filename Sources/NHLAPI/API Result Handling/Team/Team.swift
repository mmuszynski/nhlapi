//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/28/21.
//

import Foundation

public class Team: NHLAPIBaseObject,Identifiable {
    public var id: Int
    
    public var abbreviation: String?
    public var active: Bool?
    public var conference: ConferenceResult?
    public var division: DivisionResult?
    public var firstYearOfPlay: String?
    public var franchise: FranchiseResult?
    public var link: String
    public var locationName: String?
    public var logoURL: URL
    public var name: String
    public var officialSiteUrl: String?
    public var shortName: String?
    public var teamName: String?
    public var venue: VenueResult?
    
    internal init(_ apiResult: TeamResult) {
        id = apiResult.id
        
        abbreviation = apiResult.abbreviation
        active = apiResult.active
        conference = apiResult.conference
        division = apiResult.division
        firstYearOfPlay = apiResult.firstYearOfPlay
        franchise = apiResult.franchise
        link = apiResult.link
        locationName = apiResult.locationName
        logoURL = apiResult.logoURL
        name = apiResult.name
        officialSiteUrl = apiResult.officialSiteUrl
        shortName = apiResult.shortName
        teamName = apiResult.teamName
        venue = apiResult.venue
    }
    
    private func setAttributes(from otherTeam: Team) {
        id = otherTeam.id
        
        abbreviation = otherTeam.abbreviation
        active = otherTeam.active
        conference = otherTeam.conference
        division = otherTeam.division
        firstYearOfPlay = otherTeam.firstYearOfPlay
        franchise = otherTeam.franchise
        link = otherTeam.link
        locationName = otherTeam.locationName
        logoURL = otherTeam.logoURL
        name = otherTeam.name
        officialSiteUrl = otherTeam.officialSiteUrl
        shortName = otherTeam.shortName
        teamName = otherTeam.teamName
        venue = otherTeam.venue
    }
    
    /// Resolves certain optional properties by using the Team endpoint
    ///
    /// Often, when a team has been loaded by a different endpoint (and called without `Expand` options), data will be missing from the `Team` object.
    ///
    /// For example, the `Schedule` endpoint only loads attributes for the team's `name`, `id`, and `link`. This function will resolve further information (though it is not guaranteed to be exhaustive).
    ///
    /// - Warning: This needs a better name, and it will be replaced when I come up with something more descriptive
    public func resolveMoreFully() async throws {
        let fullTeamInfo = try await Teams.fetchTeamWith(id: self.id)
        self.setAttributes(from: fullTeamInfo)
    }
}
