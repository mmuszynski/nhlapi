//
//  Conference.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

@available(*, unavailable, renamed: "ConferenceResult")
public struct Conference {}

public struct ConferenceResult: Codable {
    var id: Int?
    var name: String?
    var link: String?
    var shortName: String?
    var active: Bool?
    var abbreviation: String?
}
