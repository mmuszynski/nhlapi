//
//  ConferencesResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct ConferencesResult: Codable {
    var copyright: String?
    var conferences: [ConferenceResult]
}

extension ConferencesResult: SimpleCollection {
    public var elements: [ConferenceResult] {
        return conferences
    }
}
