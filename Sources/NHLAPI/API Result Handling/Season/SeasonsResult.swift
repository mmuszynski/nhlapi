//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation

/// The object returned by a call to the Seasons endpoint
///
///  https://statsapi.web.nhl.com/api/v1/seasons
public struct SeasonsResult: NHLAPIResult {
    
    /// The copyright string
    var copyright: String?
    var seasons: [SeasonResult]
    public var wait: Int?
}
