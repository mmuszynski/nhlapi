//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//
import Foundation
import Combine

/// Publishers used to get `SeasonResult`s
/// Organized as static functions since they are used to provide a Season object
extension SeasonResult {
    /// A publisher that fetches the current `Season`
    /// - Returns: A `Publisher` that can be used to fetch the current `Season` result
    public static func publisherForCurrentSeason() -> AnyPublisher<SeasonResult, Error> {
        return URLSession.shared
            .dataTaskPublisher(for: Endpoint.currentSeason.url)
            .map { $0.data }
            .decode(type: SeasonsResult.self, decoder: NHLAPI.decoder)
            .tryMap {
                guard let season = $0.seasons.first else {
                    throw FetchError.noSeason
                }
                return season
            }
            .eraseToAnyPublisher()
    }
    
    /// A publisher that fetches all of the `Season`s that have ever occurred in the NHL
    /// - Returns: A `Publisher` that can be used to fetch an array of `Season` results
    public static func publisherForAllSeasons() -> AnyPublisher<Array<SeasonResult>, Error> {
        return URLSession.shared
            .dataTaskPublisher(for: Endpoint.seasons.url)
            .map { $0.data }
            .decode(type: SeasonsResult.self, decoder: NHLAPI.decoder)
            .map { $0.seasons }
            .eraseToAnyPublisher()
    }
    
    /// A publisher that fetches a specific `Season`
    /// - Returns: A `Publisher` that can be used to fetch a `Season` result for a given id
    public static func publisher(forSeason id: String) -> AnyPublisher<SeasonResult, Error> {
        return URLSession.shared
            .dataTaskPublisher(for: Endpoint.season(id: id).url)
            .map { $0.data }
            .decode(type: SeasonsResult.self, decoder: NHLAPI.decoder)
            .tryMap {
                guard let season = $0.seasons.first else {
                    throw FetchError.noSeason
                }
                return season
            }
            .eraseToAnyPublisher()
    }
}
