//
//  SeasonAPIResult.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation

@available(*, unavailable, renamed: "SeasonResult")
struct SeasonAPIResult {}

/// The structure that defines an NHL season as returned by the seasons endpoint
///
/// An example season structure
/* {
   "copyright" : "NHL and the NHL Shield are registered trademarks of the National Hockey League. NHL and NHL team marks are the property of the NHL and its teams. © NHL 2019. All Rights Reserved.",
   "seasons" : [ {
     "seasonId" : "20172018",
     "regularSeasonStartDate" : "2017-10-04",
     "regularSeasonEndDate" : "2018-04-08",
     "seasonEndDate" : "2018-06-07",
     "numberOfGames" : 82,
     "tiesInUse" : false,
     "olympicsParticipation" : false,
     "conferencesInUse" : true,
     "divisionsInUse" : true,
     "wildCardInUse" : true
   } ]
 }
 */
public struct SeasonResult: Codable {
    /// Errors that arise when the season information is fetched
    enum FetchError: Error {
        case noSeason
    }
    
    /// A string describing the season (e.g. "20172018")
    var seasonId: String
    
    /// A string describing the start date for the season. API result.
    public var regularSeasonStartDate: Date
    
    /// A string describing the end date for the regular season. API result..
    public var regularSeasonEndDate: Date
    
    /// A string describing the end of the season. API Result.
    public var seasonEndDate: Date
    
    /// The number of games in the season. API Result.
    var numberOfGames: Int
    
    /// Whether ties are used in the season. API Result.
    var tiesInUse: Bool
    
    /// Whether or not the players participated in the Olympic Games that season. API Result.
    var olympicsParticipation: Bool
    
    /// Whether or not the season used conferences. API Result.
    var conferencesInUse: Bool
    
    /// Whether or not the season used divisions. API Result.
    var divisionsInUse: Bool
    
    /// Whether or not the season used wild card playoff teams. API Result.
    var wildCardInUse: Bool
}
