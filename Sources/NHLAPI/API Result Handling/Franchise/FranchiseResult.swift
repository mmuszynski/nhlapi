//
//  Franchise.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

@available(*, unavailable, renamed: "FranchiseResult")
public struct Franchise {}

public struct FranchiseResult: Codable {
    var franchiseId: Int
    var teamName: String
    var link: String?
    var officialSiteUrl: String?
    
    var firstSeasonId: Int?
    var mostRecentTeamId: Int?
    var locationName: String?
}
