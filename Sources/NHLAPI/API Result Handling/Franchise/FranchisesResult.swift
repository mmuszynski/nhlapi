//
//  FranchisesResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct FranchisesResult: Codable {
    var copyright: String?
    var franchises: [FranchiseResult]
}

extension FranchisesResult: SimpleCollection {
    public var elements: [FranchiseResult] {
        return franchises
    }
}
