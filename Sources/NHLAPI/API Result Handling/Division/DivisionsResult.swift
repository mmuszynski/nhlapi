//
//  DivisionsResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct DivisionsResult: Codable {
    var copyright: String?
    var divisions: [DivisionResult]
}

extension DivisionsResult: SimpleCollection {
    public var elements: [DivisionResult] {
        return divisions
    }
}

