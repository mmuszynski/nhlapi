//
//  Division.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

@available(*, unavailable, renamed: "DivisionResult")
public struct Division {}

public struct DivisionResult: Codable {
    var id: Int?
    var name: String?
    var nameShort: String?
    var link: String?
    var abbreviation: String?
}
