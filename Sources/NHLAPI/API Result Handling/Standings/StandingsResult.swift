//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/20/21.
//

import Foundation
import Combine

struct StandingsResult: Codable {
    
}

extension StandingsResult {
    /// A `Publisher` that returns a `StandingsResult` with the given season id
    /// - Parameter teamIDs: An array of integers representing the teams to be returned
    /// - Returns: A `Publihser` that returns an `Array` of `Team` objects on completion
    public static func publisher(season seasonId: Int) -> AnyPublisher<StandingsResult, Error> {
        return URLSession.shared.dataTaskPublisher(for: Endpoint.standings.url)
            .map { $0.data }
            .decode(type: StandingsResult.self, decoder: NHLAPI.decoder)
            .eraseToAnyPublisher()
    }
}
