//
//  LiveFeedGameDataGoalieStats.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct BoxscorePlayerGoalieStats: Codable {
    public var timeOnIce: String
    public var assists: Int
    public var goals: Int?
    public var pim: Int?
    public var shots: Int?
    public var saves: Int?
    public var powerPlaySaves: Int?
    public var shortHandedSaves: Int?
    public var evenSaves: Int?
    public var shortHandedShotsAgainst: Int?
    public var evenShotsAgainst: Int?
    public var powerPlayShotsAgainst: Int?
    public var decision: String?
    public var savePercentage: Double?
    public var shortHandedSavePercentage: Double?
    public var evenStrengthSavePercentage: Double?
}
