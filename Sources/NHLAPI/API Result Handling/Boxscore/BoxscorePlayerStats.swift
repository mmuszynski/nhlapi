//
//  BoxscorePlayerStats.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct BoxscorePlayerStats: Codable {
    var skaterStats: BoxscorePlayerSkaterStats?
    var goalieStats: BoxscorePlayerGoalieStats?
}
