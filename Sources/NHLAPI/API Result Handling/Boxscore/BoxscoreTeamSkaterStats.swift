//
//  TeamSkaterStats.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct BoxscoreTeamSkaterStats: Codable {
    var goals: Int
    var pim: Int
    var shots: Int?
    var powerPlayPercentage: String?
    var powerPlayGoals: Int?
    var powerPlayOpportunities: Int?
    var faceOffWinPercentage: String?
    var blocked: Int?
    var takeaways: Int?
    var giveaways: Int?
    var hits: Int?
}
