//
//  BosxcorePenaltyBoxInfo.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/25/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

//"id" : 8477482,
//"timeRemaining" : "01:46",
//"active" : true

struct BoxscorePenaltyBoxInfo: Codable {
    var id: Int
    var timeRemaining: String
    var active: Bool
}
