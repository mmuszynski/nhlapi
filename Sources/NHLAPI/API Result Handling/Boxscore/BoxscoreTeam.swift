//
//  BoxscoreTeam.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct BoxscoreTeam: Codable {
    public var team: TeamResult
    
    /// A dictionary containing one item with the key "teamSkaterStats"
    ///
    /// `BoxscoreTeamSkaterStats` is buried in `teamSkaterStats`, which is treated as a string here.
    var teamStats: [String: BoxscoreTeamSkaterStats]
    var skaterStats: BoxscoreTeamSkaterStats {
        return teamStats["teamSkaterStats"]!
    }
    
    var players: [String: BoxscorePlayer]
    
    /// The identifiers for the goalies on the team (on the ice? who have just played?)
    var goalies: [Int]
    
    /// The identifiers for the skaters in the game
    var skaters: [Int]
    
    /// The identifiers for the players (goalie or skater) who are currently on the ice
    var onIce: [Int]
    
    /// Additional on-ice data for players includeing shift duration and stamina
    var onIcePlus: [BoxscorePlayerOnIceData]
    
    /// The indentifiers for players who have been scratched for the game
    var scratches: [Int]
    
    /// Information for players in the penalty box
    var penaltyBox: [BoxscorePenaltyBoxInfo]
    
    /// Coaches for the team
    ///
    /// Looks like maybe only the head coach?
    var coaches: [NHLCoach]
}

struct BoxscorePlayerOnIceData: Codable {
    var playerId: Int
    var shiftDuration: Int
    var stamina: Int
}
