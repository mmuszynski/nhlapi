//
//  LiveFeedGameDataSkaterStats.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct BoxscorePlayerSkaterStats: Codable {
    public var timeOnIce: String
    public var assists: Int
    public var goals: Int
    public var shots: Int?
    public var hits: Int?
    public var powerPlayGoals: Int?
    public var powerPlayAssists: Int?
    public var penaltyMinutes: Int?
    public var faceOffWins: Int?
    public var faceoffTaken: Int?
    public var takeaways: Int?
    public var giveaways: Int?
    public var shortHandedGoals: Int?
    public var shortHandedAssists: Int?
    public var blocked: Int?
    public var plusMinus: Int?
    public var evenTimeOnIce: String?
    public var powerPlayTimeOnIce: String?
    public var shortHandedTimeOnIce: String?
}
