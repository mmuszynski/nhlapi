//
//  BoxscorePlayer.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct BoxscorePlayer: Codable {
    public var jerseyNumber: String?
    public var person: NHLPerson
    public var position: NHLPosition
    var stats: BoxscorePlayerStats
    
    public var skaterStats: BoxscorePlayerSkaterStats? { self.stats.skaterStats }
    public var goalieStats: BoxscorePlayerGoalieStats? { self.stats.goalieStats }
}
