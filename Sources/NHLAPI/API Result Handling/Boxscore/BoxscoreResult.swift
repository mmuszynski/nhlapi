//
//  Boxscore.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct BoxscoreResult: Codable {
    var teams: [String: BoxscoreTeam]
    public var homeTeam: BoxscoreTeam {
        return teams["home"]!
    }
    public var awayTeam: BoxscoreTeam {
        return teams["away"]!
    }
    
    /// Officials for the game
    var officials: [NHLOfficial]
    
    /*
     var goals: Int
     var pim: Int
     var shots: Int
     var powerPlayPercentage: String
     var powerPlayGoals: Int
     var powerPlayOpportunities: Int
     var faceOffWinPercentage: String
     var blocked: Int
     var takeaways: Int
     var giveaways: Int
     var hits: Int
     */
    
    public var homeTeamGoals: Int {
        return self.homeTeam.skaterStats.goals
    }
    
    public var homeTeamAssists: Int {
        return self.homeTeam.players.map { $0.value }.map { $0.stats }.compactMap { $0.goalieStats }.map { $0.assists }.reduce(0,+) +
            self.homeTeam.players.map { $0.value }.map { $0.stats }.compactMap { $0.skaterStats }.map { $0.assists }.reduce(0,+)
    }
    
    public var homeTeamShots: Int? {
        return self.homeTeam.skaterStats.shots
    }
    
    public var homeTeamPPG: Int? {
        return self.homeTeam.skaterStats.powerPlayGoals
    }
    
    public var homeTeamBlocks: Int? {
        return self.homeTeam.skaterStats.blocked
    }
    
    public var homeTeamGiveaways: Int? {
        return self.homeTeam.skaterStats.giveaways
    }
    
    public var homeTeamHits: Int? {
        return self.homeTeam.skaterStats.hits
    }
    
    public var homeTeamPim: Int {
        return self.homeTeam.skaterStats.pim
    }
    
    public var homeTeamPowerPlays: Int? {
        return self.homeTeam.skaterStats.powerPlayOpportunities
    }
    
    public var homeTeamTakeaways: Int? {
        return self.homeTeam.skaterStats.takeaways
    }
    
    public var awayTeamGoals: Int {
        return self.awayTeam.skaterStats.goals
    }
    
    public var awayTeamAssists: Int {
        return self.awayTeam.players.map { $0.value }.map { $0.stats }.compactMap { $0.goalieStats }.map { $0.assists }.reduce(0,+) +
            self.awayTeam.players.map { $0.value }.map { $0.stats }.compactMap { $0.skaterStats }.map { $0.assists }.reduce(0,+)
    }
    
    public var awayTeamShots: Int? {
        return self.awayTeam.skaterStats.shots
    }
    
    public var awayTeamPPG: Int? {
        return self.awayTeam.skaterStats.powerPlayGoals
    }
    
    public var awayTeamBlocks: Int? {
        return self.awayTeam.skaterStats.blocked
    }
    
    public var awayTeamGiveaways: Int? {
        return self.awayTeam.skaterStats.giveaways
    }
    
    public var awayTeamHits: Int? {
        return self.awayTeam.skaterStats.hits
    }
    
    public var awayTeamPim: Int {
        return self.awayTeam.skaterStats.pim
    }
    
    public var awayTeamPowerPlays: Int? {
        return self.awayTeam.skaterStats.powerPlayOpportunities
    }
    
    public var awayTeamTakeaways: Int? {
        return self.awayTeam.skaterStats.takeaways
    }
    
    public var homeTeamSkaters: [BoxscorePlayer] {
        self.homeTeam.players.values.filter { $0.stats.skaterStats != nil }
    }
    
    public var awayTeamSkaters: [BoxscorePlayer] {
        self.awayTeam.players.values.filter { $0.stats.skaterStats != nil }
    }
    
    public var homeTeamGoalies: [BoxscorePlayer] {
        self.homeTeam.players.values.filter { $0.stats.goalieStats != nil }
    }
    
    public var awayTeamGoalies: [BoxscorePlayer] {
        self.awayTeam.players.values.filter { $0.stats.goalieStats != nil }
    }
}
