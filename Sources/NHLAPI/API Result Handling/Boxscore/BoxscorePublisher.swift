//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation
import Combine

extension BoxscoreResult {
    public typealias PublisherType = AnyPublisher<BoxscoreResult, Error>

    /// A publisher that will retrieve a `BoxscoreResult`.
    ///
    /// Creates a `Publisher` that will retrieve a single boxscore. Does not specify which thread to receive on.
    ///
    /// - Warning: You must specify `.receive(on: Scheduler)` in order to successfully download data.
    /// - Parameter gameID: The ID of the game to be fetched
    public static func publisher(for gameID: Int) -> PublisherType {
        return URLSession.shared.dataTaskPublisher(for: Endpoint.liveFeed(game: gameID).url)
            .map(\.data)
            .decode(type: BoxscoreResult.self, decoder: NHLAPI.decoder)
            .eraseToAnyPublisher()
    }
}
