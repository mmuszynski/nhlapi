//
//  NHLOfficial.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct NHLOfficial: Codable {
    var official: NHLPerson
    var officialType: String
}
