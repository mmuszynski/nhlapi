//
//  NHLPosition.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct NHLPosition: Codable {
    public var code: String
    public var name: String
    public var type: String
    public var abbreviation: String
}
