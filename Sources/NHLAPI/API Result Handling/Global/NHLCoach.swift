//
//  NHLCoach.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct NHLCoach: Codable {
    var name: String? {
        return person.fullName
    }
    
    var coachingPosition: String {
        return position.name
    }
    
    var person: NHLPerson
    var position: NHLPosition
}
