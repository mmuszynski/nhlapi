//
//  TimeZone.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct NHLTimeZone: Codable {
    ///A full description of the `TimeZone`
    var id: String
    
    ///Offset from GMT
    var offset: Int
    
    ///Short description of the `TimeZone`
    var tz: String
}
