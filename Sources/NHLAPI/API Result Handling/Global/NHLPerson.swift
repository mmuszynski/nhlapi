//
//  NHLPerson.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct NHLPerson: Codable {
    public var id: Int?
    public var fullName: String?
    public var link: String
    
    ///Predominant hand (e.g. "L" or "R")
    public var shootsCatches: String?
    
    ///Unknown?
    public var rosterStatus: String?
}
