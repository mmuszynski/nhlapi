//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/15/21.
//

import Foundation
import Combine

extension VenueResult {
    /// A `Publisher` that returns a Venue with the given id
    /// - Parameter id: An array of integers representing the teams to be returned
    /// - Returns: A `Publisher` that returns a `Venue` object
    public static func publisher(id venueID: Int) -> AnyPublisher<VenueResult?, Error> {
        return URLSession.shared.dataTaskPublisher(for: Endpoint.venue(id: venueID).url)
            .map { $0.data }
            .decode(type: VenuesResult.self, decoder: NHLAPI.decoder)
            .map { $0.venues.first }
            .eraseToAnyPublisher()
    }
}
