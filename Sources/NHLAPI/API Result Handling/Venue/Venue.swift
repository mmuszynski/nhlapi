//
//  Venue.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

@available(*, unavailable, renamed: "GameVenue")
public struct Venue {}

public struct VenueResult: Codable {
    var id: Int?
    var city: String?
    var name: String?
    var link: String?
    var appEnabled: String?
    
    var timeZone: NHLTimeZone?
}
