//
//  VenuesResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/10/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct VenuesResult: Codable {
    var copyright: String?
    var venues: [VenueResult]
}

extension VenuesResult: SimpleCollection {
    public var elements: [VenueResult] {
        return venues
    }
}
