//
//  GameVenue.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 11/8/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

@available(*, unavailable, renamed: "GameVenueResult")
struct GameVenue {}

struct GameVenueResult: Codable {
    var name: String
    var link: String
    var id: Int?
    var city: String?
    var timeZone: TimeZone?
}
