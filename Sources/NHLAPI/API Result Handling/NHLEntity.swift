//
//  NHLEntity.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/9/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

class NHLEntity: Codable, CustomDebugStringConvertible {
    var id: Int
    var name: String
    var link: String
    
    var debugDescription: String {
        return "NHLAPIEntity id \(id), name \(name), link: \(link)"
    }
}
