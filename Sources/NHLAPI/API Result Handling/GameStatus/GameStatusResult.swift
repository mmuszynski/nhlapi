//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/15/21.
//

import Foundation

//https://statsapi.web.nhl.com/api/v1/gameStatus
struct GameStatusResult: Codable {
    var code: String
    var abstractGameState: String
    var detailedState: String
    var baseballCode: String
    var startTimeTBD: Bool
}
