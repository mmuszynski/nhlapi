//
//  GameStatus.swift
//  NHLStandings
//
//  Created by Mike Muszynski on 11/8/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct GameStatus: Codable {
    var abstractGameState: String
    var codedGameState: String
    var detailedState: String
    var statusCode: String
    var startTimeTBD: Bool
}
