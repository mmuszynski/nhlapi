//
//  File.swift
//  
//
//  Created by Mike Muszynski on 2/13/21.
//

import Foundation
import Combine

extension LiveFeedResult {
    /// A publisher that will retrieve a `LiveFeedResult`.
    ///
    /// Creates a `Publisher` that will retrieve a single `LiveFeedResult`. Does not specify which thread to receive on.
    ///
    /// - Warning: You must specify `.receive(on: Scheduler)` in order to successfully download data.
    /// - Parameter gameID: The ID of the game to be fetched
    public static func publisher(for gameID: Int) -> AnyPublisher<LiveFeedResult, Error> {
        return URLSession.shared.dataTaskPublisher(for: Endpoint.liveFeed(game: gameID).url)
            .map { $0.data }
            .decode(type: LiveFeedResult.self, decoder: NHLAPI.decoder)
            .eraseToAnyPublisher()
    }
}
