//
//  LiveFeedMetaData.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedMetaData: Codable {
    
    ///Common across the NHLAPI. Probably the cache time for the result.
    var wait: Int
    
    ///Likely the timestamp for the last update provided
    var timeStamp: String
}
