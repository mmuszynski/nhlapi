//
//  LiveFeedResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct LiveFeedResult: NHLAPIResult {
    public var wait: Int?
    
    ///The result's copyright notice
    var copyright: String?
    
    ///The game's id
    var gamePk: Int
    
    ///The link used to to retrieve this result
    var link: String
    
    ///Metadata about the `LiveFeedResult`
    var metaData: LiveFeedMetaData
    
    ///Data about the game
    var gameData: LiveFeedGameData
    
    ///Live play data generated during the game
    var liveData: LiveFeedLiveData
    
    var plays: [GamePlay] {
        return liveData.plays.allPlays
    }
    
    public func plays(forPeriod index: Int) -> [GamePlay] {
        return liveData.plays.forPeriod(index)
    }
    
    public var boxscore: BoxscoreResult {
        return self.liveData.boxscore
    }
    
    public var linescore: Linescore {
        return self.liveData.linescore
    }
    
    public var allPlays: [GamePlay] {
        return self.plays
    }
    
    public var isFinal: Bool {
        return self.gameData.status.detailedState == "Final"
    }
    
    public var isUpcoming: Bool {
        return self.gameData.status.detailedState == "Scheduled"
    }
    
    public var isInProgress: Bool {
        return !self.isFinal && !self.isUpcoming
    }
    
    public var dateTime: String {
        return self.gameData.datetime.dateTime
    }
    
    public var homeTeam: TeamResult {
        return self.gameData.teams.home
    }
    
    public var awayTeam: TeamResult {
        return self.gameData.teams.away
    }
    
    public var homeTeamName: String {
        return self.gameData.teams.home.name
    }
    
    public var awayTeamName: String {
        return self.gameData.teams.away.name
    }
    
    public var homeTeamShortName: String? {
        return self.gameData.teams.home.abbreviation
    }
    
    public var awayTeamShortName: String? {
        return self.gameData.teams.away.abbreviation
    }
    
    public var homeTeamID: Int {
        return self.gameData.teams.home.id
    }
    
    public var awayTeamID: Int {
        return self.gameData.teams.away.id
    }
    
    public var shotsByPeriod: [Int] {
        return []
    }
}
