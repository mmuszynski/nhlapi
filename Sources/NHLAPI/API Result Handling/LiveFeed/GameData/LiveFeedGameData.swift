//
//  LiveFeedGameData.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedGameData: Codable {
    
    /// Basic Game information
    var game: LiveFeedGameDataGameInfo
    
    /// Start and end times for the game
    var datetime: LiveFeedGameDataDateTime
    
    /// The game's status
    var status: GameStatus
    
    /// The teams involved in the game
    var teams: LiveFeedGameDataTeams
    
    /// The players involved in the game
    var players: [String:LiveFeedGameDataPlayer]
    
    /// The venue hosting the game
    var venue: VenueResult?
}
