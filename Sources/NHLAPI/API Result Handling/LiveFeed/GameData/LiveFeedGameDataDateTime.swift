//
//  LiveFeedGameDataDateTime.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedGameDataDateTime: Codable {
    ///Probably the scheduled start time of the game.
    var dateTime: String
    
    ///Probably the time the game finished. Not present if the game has not ended.
    var endDateTime: String?
}
