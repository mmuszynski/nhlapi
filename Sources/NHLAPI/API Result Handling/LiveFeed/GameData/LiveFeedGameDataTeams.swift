//
//  LiveFeedGameDataTeams.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedGameDataTeams: Codable {
    /// The home team object
    var home: TeamResult
    
    /// The away team object
    var away: TeamResult
}
