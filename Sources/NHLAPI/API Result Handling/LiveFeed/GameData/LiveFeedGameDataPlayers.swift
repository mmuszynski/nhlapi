//
//  LiveFeedGameDataPlayers.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedGameDataPlayer: Codable {
    var id: Int
    var fullName: String
    var link: String
    var firstName: String
    var lastName: String
    var primaryNumber: String?
    var birthDate: String
    var currentAge: Int?
    var birthCity: String?
    var birthCountry: String?
    var nationality: String?
    var height: String?
    var weight: Int?
    var active: Bool
    var alternateCaptain: Bool?
    var captain: Bool?
    var rookie: Bool
    var shootsCatches: String?
    var rosterStatus: String
    var currentTeam: TeamResult?
    var primaryPosition: NHLPosition
}
