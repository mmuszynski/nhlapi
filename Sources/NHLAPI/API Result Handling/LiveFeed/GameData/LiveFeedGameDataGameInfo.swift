//
//  LiveFeedGameInfo.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedGameDataGameInfo: Codable {
    ///The game ID
    var pk: Int
    
    ///A description of the season
    var season: String
    
    ///The type of game (e.g. "R" is for Regular Season, "PR" for preseason)
    var type: String
}
