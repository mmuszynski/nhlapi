//
//  GamePlayDetail.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/12/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct GamePlayDetail: Codable, Hashable, Equatable {
    var eventIdx: Int
    var eventId: Int
    var period: Int
    var periodType: String
    var ordinalNum: String
    var periodTime: String
    var periodTimeRemaining: String
    var dateTime: String
    
    var goals: [String: Int]
    var homeGoals: Int {
        return goals["home"]!
    }
    var awayGoals: Int {
        return goals["away"]!
    }
}
