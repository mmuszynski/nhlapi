//
//  GamePlayPeriodResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/12/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct GamePlayPeriodDetail: Codable {
    var startIndex: Int
    var endIndex: Int
    var plays: [Int]
}
