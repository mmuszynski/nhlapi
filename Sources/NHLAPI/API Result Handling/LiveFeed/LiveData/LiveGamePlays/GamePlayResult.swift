//
//  GamePlayResult.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/12/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct GamePlayResult: Codable {
    var event: String
    var eventCode: String
    var eventTypeId: String
    var description: String
    public var secondaryType: String?
    var strength: [String:String]?
}

/*
 "event" : "Goal",
 "eventCode" : "COL75",
 "eventTypeId" : "GOAL",
 "description" : "Joonas Donskoi (1) Tip-In, assists: Samuel Girard (1), Andre Burakovsky (1)",
 "secondaryType" : "Tip-In",
 "strength" : {
   "code" : "PPG",
   "name" : "Power Play"
 },
 */
