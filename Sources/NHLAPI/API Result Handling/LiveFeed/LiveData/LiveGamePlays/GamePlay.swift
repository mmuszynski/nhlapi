//
//  GamePlay.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct GamePlay: Codable {
    public enum PlayType {
        case shot, hit, blockedShot, goal, penalty, unknown
    }
    
    public var players: [GamePlayPlayerDetail]?
    public var result: GamePlayResult
    public var about: GamePlayDetail
    public var coordinates: GamePlayCoordinates
    var team: TeamResult?
    
    public var playType: PlayType {
        switch self.result.eventTypeId {
        case "SHOT":
            return .shot
        case "BLOCKED_SHOT":
            return .blockedShot
        case "GOAL":
            return .goal
        case "HIT":
            return .hit
        case "PENALTY":
            return .penalty
        default:
            return .unknown
        }
    }
    
    public var period: Int {
        return about.period
    }
    
    public var periodTime: String {
        return about.periodTime
    }
    
    public var periodOrdinal: String {
        return about.ordinalNum
    }
    
    public var playDescription: String {
        return result.description
    }
    
    public var teamName: String? {
        return team?.teamName
    }
    
    public var teamID: Int? {
        return team?.id
    }
}

extension GamePlay: Hashable {
    public static func == (lhs: GamePlay, rhs: GamePlay) -> Bool {
        return lhs.about == rhs.about
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.coordinates)
        hasher.combine(self.about)
    }
}

extension GamePlay {
    public static var test: GamePlay {
        let string = """
            {
               "players" : [ {
                 "player" : {
                   "id" : 8475820,
                   "fullName" : "Joonas Donskoi",
                   "link" : "/api/v1/people/8475820"
                 },
                 "playerType" : "Scorer",
                 "seasonTotal" : 1
               }, {
                 "player" : {
                   "id" : 8479398,
                   "fullName" : "Samuel Girard",
                   "link" : "/api/v1/people/8479398"
                 },
                 "playerType" : "Assist",
                 "seasonTotal" : 1
               }, {
                 "player" : {
                   "id" : 8477444,
                   "fullName" : "Andre Burakovsky",
                   "link" : "/api/v1/people/8477444"
                 },
                 "playerType" : "Assist",
                 "seasonTotal" : 1
               }, {
                 "player" : {
                   "id" : 8479496,
                   "fullName" : "David Rittich",
                   "link" : "/api/v1/people/8479496"
                 },
                 "playerType" : "Goalie"
               } ],
              "result" : {
                "event" : "Goal",
                "eventCode" : "COL75",
                "eventTypeId" : "GOAL",
                "description" : "Joonas Donskoi (1) Tip-In, assists: Samuel Girard (1), Andre Burakovsky (1)",
                "secondaryType" : "Tip-In",
                "strength" : {
                  "code" : "PPG",
                  "name" : "Power Play"
                },
                "gameWinningGoal" : false,
                "emptyNet" : false
              },
              "about" : {
                "eventIdx" : 34,
                "eventId" : 75,
                "period" : 1,
                "periodType" : "REGULAR",
                "ordinalNum" : "1st",
                "periodTime" : "05:59",
                "periodTimeRemaining" : "14:01",
                "dateTime" : "2019-10-04T01:28:45Z",
                "goals" : {
                  "away" : 0,
                  "home" : 1
                }
              },
              "coordinates" : {
                "x" : -65.0,
                "y" : 6.0
              },
              "team" : {
                "id" : 21,
                "name" : "Colorado Avalanche",
                "link" : "/api/v1/teams/21",
                "triCode" : "COL"
              }
            }
            """
        
        let data = string.data(using: .utf8)!
        return try! NHLAPI.decode(GamePlay.self, from: data)
    }
}
