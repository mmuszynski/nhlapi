//
//  GamePlayCoordinates.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/12/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct GamePlayCoordinates: Codable {
    public var x: Int?
    public var y: Int?
}

extension GamePlayCoordinates: Hashable {
    public static func == (lhs: GamePlayCoordinates, rhs: GamePlayCoordinates) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(x)
        hasher.combine(y)
    }
}
