//
//  GamePlayPlayerDetail.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 12/12/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

public struct GamePlayPlayerDetail: Codable {
    var player: NHLPerson
    var playerType: String
    var seasonTotal: Int?
}
