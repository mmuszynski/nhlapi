//
//  LiveFeedLiveData.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedLiveData: Codable {
    
    var plays: LiveFeedLiveDataPlays
    var linescore: Linescore
    var boxscore: BoxscoreResult
    
    var decisions: [String:NHLPerson]
}
