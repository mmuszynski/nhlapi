//
//  LiveFeedLiveDataPlay.swift
//  NHLAPI
//
//  Created by Mike Muszynski on 11/24/19.
//  Copyright © 2019 Mike Muszynski. All rights reserved.
//

import Foundation

struct LiveFeedLiveDataPlays: Codable {
    ///An array of the full listing of plays in the game
    var allPlays: [GamePlay]
    
    ///An array of indices for the scoring plays in the game
    var scoringPlays: [Int]
    
    ///An array of indices for the penalty plays in the game
    var penaltyPlays: [Int]
    
    ///Convoluted way of coming up with the plays from each period
    var playsByPeriod: [GamePlayPeriodDetail]
    
    ///Plays for a given period. One-based numbering.
    func forPeriod(_ index: Int) -> [GamePlay] {
        let index = index - 1
        guard playsByPeriod.count > index, index >= 0 else { return [] }
        let range = playsByPeriod[index].startIndex..<playsByPeriod.endIndex
        return Array(self[range])
    }
}

extension LiveFeedLiveDataPlays: SimpleCollection {
    typealias Element = GamePlay
    
    var elements: [GamePlay] {
        return allPlays
    }
}
