import Cocoa
import NHLAPI
import Combine

let season = "20032004"
var cancellables: Set<AnyCancellable> = []

var games: [Linescore] = []
var teams: Set<Int> = []

var startDate: Date = .distantPast
var finalDate: Date = .distantPast
var dateRange: ClosedRange<Date> = Date.distantPast...Date.distantPast

extension Date {
    func adding(days: Int) -> Date {
        return self.addingTimeInterval(TimeInterval(days) * 24 * 60 * 60)
    }
}

extension ClosedRange where Bound == Date {
    func adding(days: Int) -> ClosedRange<Bound> {
        return (self.lowerBound.adding(days: days)...self.upperBound.adding(days: days))
    }
}

SeasonResult
    .publisher(forSeason: "20032004")
    .flatMap { season -> AnyPublisher<Array<ScheduleGameResult>, Error> in
        startDate = season.regularSeasonStartDate
        finalDate = season.regularSeasonEndDate

        dateRange = startDate...startDate.adding(days: 6)
        
        return ScheduleGameResult
            .publisher(for: dateRange, expanding: [.linescore])
            .eraseToAnyPublisher()
    }
    .paginate(strategy: { previousResult in
        dateRange = dateRange
            .adding(days: 7)
            .clamped(to: startDate...finalDate)

        guard dateRange.lowerBound != dateRange.upperBound else {
            return nil
        }
            
        return ScheduleGameResult
                .publisher(for: dateRange)
                .eraseToAnyPublisher()
    })
    .sink { (completion) in
        print(completion)
        print("Downloaded \(games.count) linescores")
        calculateGoldPlan()
    } receiveValue: { (value) in
        games.append(contentsOf: value.compactMap { $0.linescore })
    }.store(in: &cancellables)

func calculateGoldPlan() {
    //Extract team numbers from games
    //Probably only need one of the away or home team
    //this should have 31 teams in 2003 because of the ASG
    teams.formUnion(games.map(\.awayTeamID))
}

func formMatrix(forTeam id: Int) -> [ScheduleGameTeamRecord] {
    return []
}

func points(for record: ScheduleGameTeamRecord) -> Int {
    return 0
}

func games(for record: ScheduleGameTeamRecord) -> Int {
    return record.wins + record.losses + (record.ties ?? 0) + (record.ot ?? 0)
}
