//: [Previous](@previous)

import Foundation
import NHLAPI
import Combine

var cancellables: Set<AnyCancellable> = []
SeasonResult.publisher(forSeason: "20192020")
    .flatMap { seasonResult -> AnyPublisher<Array<ScheduleGameResult>, Error> in
        let regularSeason = seasonResult.regularSeasonStartDate...seasonResult.regularSeasonEndDate
        return ScheduleGameResult
            .publisher(forTeam: 14, dateRange: regularSeason)
            .eraseToAnyPublisher()
    }
    .replaceError(with: [])
    .flatMap { games -> AnyPublisher<Int, Never> in
        games
            .map { $0.id }
            .publisher
            .eraseToAnyPublisher()
    }
    .flatMap { id in
        BoxscoreResult
            .publisher(for: id)
            .eraseToAnyPublisher()
    }
    .sink(receiveCompletion: { _ in
    }) { box in
        print(box)
    }
    .store(in: &cancellables)

//: [Next](@next)
